﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AutoUI;

namespace DemoFramework
{
    public partial class 按钮 : Form
    {
        public 按钮()
        {
            InitializeComponent();
            this.AddAutoScale();
        }

        private void 按钮7_Click(object sender, EventArgs e)
        {
            (sender as Button).BackColor = ColorExt.GetRandomColor();
        }
    }
}