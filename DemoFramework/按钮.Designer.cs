﻿namespace DemoFramework
{
    partial class 按钮
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.按钮7 = new AutoUI.系统扩展控件.按钮();
            this.按钮3 = new AutoUI.系统扩展控件.按钮();
            this.按钮6 = new AutoUI.系统扩展控件.按钮();
            this.按钮5 = new AutoUI.系统扩展控件.按钮();
            this.按钮4 = new AutoUI.系统扩展控件.按钮();
            this.按钮2 = new AutoUI.系统扩展控件.按钮();
            this.按钮1 = new AutoUI.系统扩展控件.按钮();
            this.SuspendLayout();
            // 
            // 按钮7
            // 
            this.按钮7.AngleOffset = 300F;
            this.按钮7.BackColor = System.Drawing.Color.Blue;
            this.按钮7.CountLine = 8;
            this.按钮7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.按钮7.IsAutoRotate = true;
            this.按钮7.Location = new System.Drawing.Point(583, 97);
            this.按钮7.Name = "按钮7";
            this.按钮7.RotateDegrees = 100;
            this.按钮7.RotateSpeed = 100;
            this.按钮7.Shape = AutoUI.EnumShapeType.星形;
            this.按钮7.Size = new System.Drawing.Size(205, 148);
            this.按钮7.StarScale = 0.1F;
            this.按钮7.TabIndex = 7;
            this.按钮7.Text = " ";
            this.按钮7.UseVisualStyleBackColor = false;
            this.按钮7.Click += new System.EventHandler(this.按钮7_Click);
            // 
            // 按钮3
            // 
            this.按钮3.AngleOffset = 0F;
            this.按钮3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.按钮3.CountLine = 5;
            this.按钮3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.按钮3.IsAutoRotate = false;
            this.按钮3.Location = new System.Drawing.Point(455, 33);
            this.按钮3.Name = "按钮3";
            this.按钮3.RotateDegrees = 0;
            this.按钮3.RotateSpeed = 100;
            this.按钮3.Shape = AutoUI.EnumShapeType.圆形;
            this.按钮3.Size = new System.Drawing.Size(150, 150);
            this.按钮3.StarScale = 0.5F;
            this.按钮3.TabIndex = 6;
            this.按钮3.Text = "圆";
            this.按钮3.UseVisualStyleBackColor = false;
            this.按钮3.Click += new System.EventHandler(this.按钮7_Click);
            // 
            // 按钮6
            // 
            this.按钮6.AngleOffset = 2F;
            this.按钮6.BackColor = System.Drawing.Color.DarkGreen;
            this.按钮6.CountLine = 6;
            this.按钮6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.按钮6.IsAutoRotate = false;
            this.按钮6.Location = new System.Drawing.Point(439, 225);
            this.按钮6.Name = "按钮6";
            this.按钮6.RotateDegrees = 0;
            this.按钮6.RotateSpeed = 100;
            this.按钮6.Shape = AutoUI.EnumShapeType.正多边形;
            this.按钮6.Size = new System.Drawing.Size(205, 148);
            this.按钮6.StarScale = 0.5F;
            this.按钮6.TabIndex = 5;
            this.按钮6.Text = "六边形";
            this.按钮6.UseVisualStyleBackColor = false;
            this.按钮6.Click += new System.EventHandler(this.按钮7_Click);
            // 
            // 按钮5
            // 
            this.按钮5.AngleOffset = 60F;
            this.按钮5.BackColor = System.Drawing.Color.Black;
            this.按钮5.CountLine = 3;
            this.按钮5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.按钮5.IsAutoRotate = false;
            this.按钮5.Location = new System.Drawing.Point(12, 125);
            this.按钮5.Name = "按钮5";
            this.按钮5.RotateDegrees = 0;
            this.按钮5.RotateSpeed = 100;
            this.按钮5.Shape = AutoUI.EnumShapeType.星形;
            this.按钮5.Size = new System.Drawing.Size(158, 148);
            this.按钮5.StarScale = 0.5F;
            this.按钮5.TabIndex = 4;
            this.按钮5.Text = "三角形";
            this.按钮5.UseVisualStyleBackColor = false;
            this.按钮5.Click += new System.EventHandler(this.按钮7_Click);
            // 
            // 按钮4
            // 
            this.按钮4.AngleOffset = 2F;
            this.按钮4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.按钮4.CountLine = 6;
            this.按钮4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.按钮4.IsAutoRotate = false;
            this.按钮4.Location = new System.Drawing.Point(213, 203);
            this.按钮4.Name = "按钮4";
            this.按钮4.RotateDegrees = 0;
            this.按钮4.RotateSpeed = 100;
            this.按钮4.Shape = AutoUI.EnumShapeType.圆角矩形;
            this.按钮4.Size = new System.Drawing.Size(205, 148);
            this.按钮4.StarScale = 0.5F;
            this.按钮4.TabIndex = 3;
            this.按钮4.Text = "圆角矩形";
            this.按钮4.UseVisualStyleBackColor = false;
            this.按钮4.Click += new System.EventHandler(this.按钮7_Click);
            // 
            // 按钮2
            // 
            this.按钮2.AngleOffset = 2F;
            this.按钮2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.按钮2.CountLine = 6;
            this.按钮2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.按钮2.IsAutoRotate = false;
            this.按钮2.Location = new System.Drawing.Point(230, 24);
            this.按钮2.Name = "按钮2";
            this.按钮2.RotateDegrees = 0;
            this.按钮2.RotateSpeed = 100;
            this.按钮2.Shape = AutoUI.EnumShapeType.圆形;
            this.按钮2.Size = new System.Drawing.Size(179, 148);
            this.按钮2.StarScale = 0.5F;
            this.按钮2.TabIndex = 1;
            this.按钮2.Text = "椭圆";
            this.按钮2.UseVisualStyleBackColor = false;
            this.按钮2.Click += new System.EventHandler(this.按钮7_Click);
            // 
            // 按钮1
            // 
            this.按钮1.AngleOffset = 22F;
            this.按钮1.BackColor = System.Drawing.SystemColors.ControlText;
            this.按钮1.CountLine = 5;
            this.按钮1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.按钮1.IsAutoRotate = false;
            this.按钮1.Location = new System.Drawing.Point(37, 12);
            this.按钮1.Name = "按钮1";
            this.按钮1.RotateDegrees = 0;
            this.按钮1.RotateSpeed = 100;
            this.按钮1.Shape = AutoUI.EnumShapeType.星形;
            this.按钮1.Size = new System.Drawing.Size(205, 148);
            this.按钮1.StarScale = 0.5F;
            this.按钮1.TabIndex = 0;
            this.按钮1.Text = "五角星";
            this.按钮1.UseVisualStyleBackColor = false;
            this.按钮1.Click += new System.EventHandler(this.按钮7_Click);
            // 
            // 按钮
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.按钮7);
            this.Controls.Add(this.按钮3);
            this.Controls.Add(this.按钮6);
            this.Controls.Add(this.按钮5);
            this.Controls.Add(this.按钮4);
            this.Controls.Add(this.按钮2);
            this.Controls.Add(this.按钮1);
            this.Name = "按钮";
            this.Text = "按钮";
            this.ResumeLayout(false);

        }

        #endregion

        private AutoUI.系统扩展控件.按钮 按钮1;
        private AutoUI.系统扩展控件.按钮 按钮2;
        private AutoUI.系统扩展控件.按钮 按钮4;
        private AutoUI.系统扩展控件.按钮 按钮5;
        private AutoUI.系统扩展控件.按钮 按钮6;
        private AutoUI.系统扩展控件.按钮 按钮3;
        private AutoUI.系统扩展控件.按钮 按钮7;
    }
}