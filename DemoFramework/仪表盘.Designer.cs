﻿namespace DemoFramework
{
    partial class 仪表盘
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(仪表盘));
            System.Drawing.Drawing2D.GraphicsPath graphicsPath1 = new System.Drawing.Drawing2D.GraphicsPath();
            System.Drawing.Drawing2D.GraphicsPath graphicsPath2 = new System.Drawing.Drawing2D.GraphicsPath();
            System.Drawing.Drawing2D.GraphicsPath graphicsPath3 = new System.Drawing.Drawing2D.GraphicsPath();
            System.Drawing.Drawing2D.GraphicsPath graphicsPath4 = new System.Drawing.Drawing2D.GraphicsPath();
            System.Drawing.Drawing2D.GraphicsPath graphicsPath5 = new System.Drawing.Drawing2D.GraphicsPath();
            System.Drawing.Drawing2D.GraphicsPath graphicsPath6 = new System.Drawing.Drawing2D.GraphicsPath();
            System.Drawing.Drawing2D.GraphicsPath graphicsPath7 = new System.Drawing.Drawing2D.GraphicsPath();
            System.Drawing.Drawing2D.GraphicsPath graphicsPath8 = new System.Drawing.Drawing2D.GraphicsPath();
            System.Drawing.Drawing2D.GraphicsPath graphicsPath9 = new System.Drawing.Drawing2D.GraphicsPath();
            System.Drawing.Drawing2D.GraphicsPath graphicsPath10 = new System.Drawing.Drawing2D.GraphicsPath();
            System.Drawing.Drawing2D.GraphicsPath graphicsPath11 = new System.Drawing.Drawing2D.GraphicsPath();
            System.Drawing.Drawing2D.GraphicsPath graphicsPath12 = new System.Drawing.Drawing2D.GraphicsPath();
            System.Drawing.Drawing2D.GraphicsPath graphicsPath13 = new System.Drawing.Drawing2D.GraphicsPath();
            System.Drawing.Drawing2D.GraphicsPath graphicsPath14 = new System.Drawing.Drawing2D.GraphicsPath();
            System.Drawing.Drawing2D.GraphicsPath graphicsPath15 = new System.Drawing.Drawing2D.GraphicsPath();
            System.Drawing.Drawing2D.GraphicsPath graphicsPath16 = new System.Drawing.Drawing2D.GraphicsPath();
            System.Drawing.Drawing2D.GraphicsPath graphicsPath17 = new System.Drawing.Drawing2D.GraphicsPath();
            System.Drawing.Drawing2D.GraphicsPath graphicsPath18 = new System.Drawing.Drawing2D.GraphicsPath();
            System.Drawing.Drawing2D.GraphicsPath graphicsPath19 = new System.Drawing.Drawing2D.GraphicsPath();
            System.Drawing.Drawing2D.GraphicsPath graphicsPath20 = new System.Drawing.Drawing2D.GraphicsPath();
            this.仪表盘4 = new AutoUI.仪表盘();
            this.仪表盘2 = new AutoUI.仪表盘();
            this.仪表盘3 = new AutoUI.仪表盘();
            this.仪表盘1 = new AutoUI.仪表盘();
            this.仪表盘5 = new AutoUI.仪表盘();
            this.SuspendLayout();
            // 
            // 仪表盘4
            // 
            this.仪表盘4.CurrentDecimalPlaces = ((byte)(2));
            this.仪表盘4.CurrentValueColor = System.Drawing.Color.Blue;
            this.仪表盘4.Description = "仪表";
            this.仪表盘4.DescriptionColor = System.Drawing.Color.Black;
            this.仪表盘4.DescriptionFont = new System.Drawing.Font("宋体", 9F);
            this.仪表盘4.DescriptionPoint = ((System.Drawing.PointF)(resources.GetObject("仪表盘4.DescriptionPoint")));
            this.仪表盘4.HightAlarm = 90F;
            this.仪表盘4.Location = new System.Drawing.Point(500, 52);
            this.仪表盘4.LowAlarm = 60F;
            this.仪表盘4.MainTickColor = System.Drawing.Color.Black;
            this.仪表盘4.MainTickCount = 10;
            this.仪表盘4.MainTickLegth = 0.15F;
            graphicsPath1.FillMode = System.Drawing.Drawing2D.FillMode.Alternate;
            this.仪表盘4.MainTickPath = graphicsPath1;
            this.仪表盘4.MainTickWidth = 3;
            this.仪表盘4.Max = 100F;
            this.仪表盘4.MeterPosition = ((System.Drawing.PointF)(resources.GetObject("仪表盘4.MeterPosition")));
            this.仪表盘4.MeterRadius = 1F;
            this.仪表盘4.Min = 0F;
            this.仪表盘4.Name = "仪表盘4";
            this.仪表盘4.NeedleColor = System.Drawing.Color.Blue;
            this.仪表盘4.NeedleHight = 0.05F;
            this.仪表盘4.NeedleStytle = AutoUI.EnumStytle.Stytle2;
            this.仪表盘4.NeedleWidth = 0.5F;
            this.仪表盘4.Point_Center = ((System.Drawing.PointF)(resources.GetObject("仪表盘4.Point_Center")));
            this.仪表盘4.Radius = 150F;
            this.仪表盘4.SecondTickColor = System.Drawing.Color.Black;
            this.仪表盘4.SecondTickCount = 2;
            this.仪表盘4.SecondTickLegth = 0.1F;
            graphicsPath2.FillMode = System.Drawing.Drawing2D.FillMode.Alternate;
            this.仪表盘4.SecondTickPath = graphicsPath2;
            this.仪表盘4.SecondTickWidth = 2;
            this.仪表盘4.Size = new System.Drawing.Size(300, 300);
            this.仪表盘4.StartAngle = 0F;
            this.仪表盘4.SweepAngle = 180F;
            this.仪表盘4.TabIndex = 3;
            this.仪表盘4.Text = "仪表盘4";
            this.仪表盘4.ThirdTickColor = System.Drawing.Color.Black;
            this.仪表盘4.ThirdTickCount = 5;
            this.仪表盘4.ThirdTickLegth = 0.05F;
            graphicsPath3.FillMode = System.Drawing.Drawing2D.FillMode.Alternate;
            this.仪表盘4.ThirdTickPath = graphicsPath3;
            this.仪表盘4.ThirdTickWidth = 1;
            this.仪表盘4.TickNumberDecimalPointCount = 1;
            this.仪表盘4.TickNumberDirection = true;
            this.仪表盘4.TickNumberFont = new System.Drawing.Font("宋体", 9F);
            this.仪表盘4.TickNumberSize = 0.65F;
            this.仪表盘4.TickNumColor = System.Drawing.Color.Black;
            this.仪表盘4.TickRingAlarmColor = System.Drawing.Color.OrangeRed;
            this.仪表盘4.TickRingColor = System.Drawing.Color.Black;
            this.仪表盘4.TickRingErrorColor = System.Drawing.Color.Red;
            graphicsPath4.FillMode = System.Drawing.Drawing2D.FillMode.Alternate;
            this.仪表盘4.TickRingPath = graphicsPath4;
            this.仪表盘4.TickRingShow = true;
            this.仪表盘4.TickRingWidth = 2;
            this.仪表盘4.TickSize = 0.95F;
            this.仪表盘4.Unit = "";
            this.仪表盘4.UnitPerDegrees = 1.8F;
            this.仪表盘4.Value = 20F;
            this.仪表盘4.ValueFont = new System.Drawing.Font("宋体", 12F);
            this.仪表盘4.ValuePositionOffset = ((System.Drawing.PointF)(resources.GetObject("仪表盘4.ValuePositionOffset")));
            // 
            // 仪表盘2
            // 
            this.仪表盘2.CurrentDecimalPlaces = ((byte)(2));
            this.仪表盘2.CurrentValueColor = System.Drawing.Color.Red;
            this.仪表盘2.Description = "仪表";
            this.仪表盘2.DescriptionColor = System.Drawing.Color.Black;
            this.仪表盘2.DescriptionFont = new System.Drawing.Font("宋体", 9F);
            this.仪表盘2.DescriptionPoint = ((System.Drawing.PointF)(resources.GetObject("仪表盘2.DescriptionPoint")));
            this.仪表盘2.HightAlarm = 90F;
            this.仪表盘2.Location = new System.Drawing.Point(828, 20);
            this.仪表盘2.LowAlarm = 60F;
            this.仪表盘2.MainTickColor = System.Drawing.Color.Black;
            this.仪表盘2.MainTickCount = 10;
            this.仪表盘2.MainTickLegth = 0.15F;
            graphicsPath5.FillMode = System.Drawing.Drawing2D.FillMode.Alternate;
            this.仪表盘2.MainTickPath = graphicsPath5;
            this.仪表盘2.MainTickWidth = 3;
            this.仪表盘2.Max = 100F;
            this.仪表盘2.MeterPosition = ((System.Drawing.PointF)(resources.GetObject("仪表盘2.MeterPosition")));
            this.仪表盘2.MeterRadius = 1F;
            this.仪表盘2.Min = 0F;
            this.仪表盘2.Name = "仪表盘2";
            this.仪表盘2.NeedleColor = System.Drawing.Color.GreenYellow;
            this.仪表盘2.NeedleHight = 0.05F;
            this.仪表盘2.NeedleStytle = AutoUI.EnumStytle.Stytle1;
            this.仪表盘2.NeedleWidth = 0.5F;
            this.仪表盘2.Point_Center = ((System.Drawing.PointF)(resources.GetObject("仪表盘2.Point_Center")));
            this.仪表盘2.Radius = 150F;
            this.仪表盘2.SecondTickColor = System.Drawing.Color.Black;
            this.仪表盘2.SecondTickCount = 2;
            this.仪表盘2.SecondTickLegth = 0.1F;
            graphicsPath6.FillMode = System.Drawing.Drawing2D.FillMode.Alternate;
            this.仪表盘2.SecondTickPath = graphicsPath6;
            this.仪表盘2.SecondTickWidth = 2;
            this.仪表盘2.Size = new System.Drawing.Size(300, 300);
            this.仪表盘2.StartAngle = 330F;
            this.仪表盘2.SweepAngle = 240F;
            this.仪表盘2.TabIndex = 4;
            this.仪表盘2.Text = "仪表盘2";
            this.仪表盘2.ThirdTickColor = System.Drawing.Color.Black;
            this.仪表盘2.ThirdTickCount = 5;
            this.仪表盘2.ThirdTickLegth = 0.05F;
            graphicsPath7.FillMode = System.Drawing.Drawing2D.FillMode.Alternate;
            this.仪表盘2.ThirdTickPath = graphicsPath7;
            this.仪表盘2.ThirdTickWidth = 1;
            this.仪表盘2.TickNumberDecimalPointCount = 0;
            this.仪表盘2.TickNumberDirection = false;
            this.仪表盘2.TickNumberFont = new System.Drawing.Font("宋体", 9F);
            this.仪表盘2.TickNumberSize = 0.65F;
            this.仪表盘2.TickNumColor = System.Drawing.Color.Black;
            this.仪表盘2.TickRingAlarmColor = System.Drawing.Color.Orchid;
            this.仪表盘2.TickRingColor = System.Drawing.Color.Black;
            this.仪表盘2.TickRingErrorColor = System.Drawing.Color.RoyalBlue;
            graphicsPath8.FillMode = System.Drawing.Drawing2D.FillMode.Alternate;
            this.仪表盘2.TickRingPath = graphicsPath8;
            this.仪表盘2.TickRingShow = true;
            this.仪表盘2.TickRingWidth = 2;
            this.仪表盘2.TickSize = 0.95F;
            this.仪表盘2.Unit = "";
            this.仪表盘2.UnitPerDegrees = 2.4F;
            this.仪表盘2.Value = 20F;
            this.仪表盘2.ValueFont = new System.Drawing.Font("宋体", 12F);
            this.仪表盘2.ValuePositionOffset = ((System.Drawing.PointF)(resources.GetObject("仪表盘2.ValuePositionOffset")));
            // 
            // 仪表盘3
            // 
            this.仪表盘3.CurrentDecimalPlaces = ((byte)(2));
            this.仪表盘3.CurrentValueColor = System.Drawing.Color.Red;
            this.仪表盘3.Description = "仪表";
            this.仪表盘3.DescriptionColor = System.Drawing.Color.Black;
            this.仪表盘3.DescriptionFont = new System.Drawing.Font("宋体", 9F);
            this.仪表盘3.DescriptionPoint = ((System.Drawing.PointF)(resources.GetObject("仪表盘3.DescriptionPoint")));
            this.仪表盘3.HightAlarm = 90F;
            this.仪表盘3.Location = new System.Drawing.Point(829, 237);
            this.仪表盘3.LowAlarm = 60F;
            this.仪表盘3.MainTickColor = System.Drawing.Color.Black;
            this.仪表盘3.MainTickCount = 10;
            this.仪表盘3.MainTickLegth = 0.15F;
            graphicsPath9.FillMode = System.Drawing.Drawing2D.FillMode.Alternate;
            this.仪表盘3.MainTickPath = graphicsPath9;
            this.仪表盘3.MainTickWidth = 3;
            this.仪表盘3.Max = 100F;
            this.仪表盘3.MeterPosition = ((System.Drawing.PointF)(resources.GetObject("仪表盘3.MeterPosition")));
            this.仪表盘3.MeterRadius = 1F;
            this.仪表盘3.Min = 0F;
            this.仪表盘3.Name = "仪表盘3";
            this.仪表盘3.NeedleColor = System.Drawing.Color.HotPink;
            this.仪表盘3.NeedleHight = 0.05F;
            this.仪表盘3.NeedleStytle = AutoUI.EnumStytle.Stytle2;
            this.仪表盘3.NeedleWidth = 0.5F;
            this.仪表盘3.Point_Center = ((System.Drawing.PointF)(resources.GetObject("仪表盘3.Point_Center")));
            this.仪表盘3.Radius = 150F;
            this.仪表盘3.SecondTickColor = System.Drawing.Color.Black;
            this.仪表盘3.SecondTickCount = 2;
            this.仪表盘3.SecondTickLegth = 0.1F;
            graphicsPath10.FillMode = System.Drawing.Drawing2D.FillMode.Alternate;
            this.仪表盘3.SecondTickPath = graphicsPath10;
            this.仪表盘3.SecondTickWidth = 2;
            this.仪表盘3.Size = new System.Drawing.Size(300, 300);
            this.仪表盘3.StartAngle = 200F;
            this.仪表盘3.SweepAngle = 140F;
            this.仪表盘3.TabIndex = 5;
            this.仪表盘3.Text = "仪表盘3";
            this.仪表盘3.ThirdTickColor = System.Drawing.Color.Black;
            this.仪表盘3.ThirdTickCount = 5;
            this.仪表盘3.ThirdTickLegth = 0.05F;
            graphicsPath11.FillMode = System.Drawing.Drawing2D.FillMode.Alternate;
            this.仪表盘3.ThirdTickPath = graphicsPath11;
            this.仪表盘3.ThirdTickWidth = 1;
            this.仪表盘3.TickNumberDecimalPointCount = 1;
            this.仪表盘3.TickNumberDirection = true;
            this.仪表盘3.TickNumberFont = new System.Drawing.Font("宋体", 9F);
            this.仪表盘3.TickNumberSize = 0.65F;
            this.仪表盘3.TickNumColor = System.Drawing.Color.Black;
            this.仪表盘3.TickRingAlarmColor = System.Drawing.Color.Orange;
            this.仪表盘3.TickRingColor = System.Drawing.Color.Black;
            this.仪表盘3.TickRingErrorColor = System.Drawing.Color.SkyBlue;
            graphicsPath12.FillMode = System.Drawing.Drawing2D.FillMode.Alternate;
            this.仪表盘3.TickRingPath = graphicsPath12;
            this.仪表盘3.TickRingShow = true;
            this.仪表盘3.TickRingWidth = 2;
            this.仪表盘3.TickSize = 0.95F;
            this.仪表盘3.Unit = "";
            this.仪表盘3.UnitPerDegrees = 1.4F;
            this.仪表盘3.Value = 20F;
            this.仪表盘3.ValueFont = new System.Drawing.Font("宋体", 12F);
            this.仪表盘3.ValuePositionOffset = ((System.Drawing.PointF)(resources.GetObject("仪表盘3.ValuePositionOffset")));
            // 
            // 仪表盘1
            // 
            this.仪表盘1.CurrentDecimalPlaces = ((byte)(2));
            this.仪表盘1.CurrentValueColor = System.Drawing.Color.Black;
            this.仪表盘1.Description = "仪表";
            this.仪表盘1.DescriptionColor = System.Drawing.Color.Black;
            this.仪表盘1.DescriptionFont = new System.Drawing.Font("宋体", 9F);
            this.仪表盘1.DescriptionPoint = ((System.Drawing.PointF)(resources.GetObject("仪表盘1.DescriptionPoint")));
            this.仪表盘1.HightAlarm = 90F;
            this.仪表盘1.Location = new System.Drawing.Point(501, 366);
            this.仪表盘1.LowAlarm = 60F;
            this.仪表盘1.MainTickColor = System.Drawing.Color.Black;
            this.仪表盘1.MainTickCount = 10;
            this.仪表盘1.MainTickLegth = 0.15F;
            graphicsPath13.FillMode = System.Drawing.Drawing2D.FillMode.Alternate;
            this.仪表盘1.MainTickPath = graphicsPath13;
            this.仪表盘1.MainTickWidth = 3;
            this.仪表盘1.Max = 100F;
            this.仪表盘1.MeterPosition = ((System.Drawing.PointF)(resources.GetObject("仪表盘1.MeterPosition")));
            this.仪表盘1.MeterRadius = 1F;
            this.仪表盘1.Min = 0F;
            this.仪表盘1.Name = "仪表盘1";
            this.仪表盘1.NeedleColor = System.Drawing.Color.Green;
            this.仪表盘1.NeedleHight = 0.05F;
            this.仪表盘1.NeedleStytle = AutoUI.EnumStytle.Stytle1;
            this.仪表盘1.NeedleWidth = 0.5F;
            this.仪表盘1.Point_Center = ((System.Drawing.PointF)(resources.GetObject("仪表盘1.Point_Center")));
            this.仪表盘1.Radius = 150F;
            this.仪表盘1.SecondTickColor = System.Drawing.Color.Black;
            this.仪表盘1.SecondTickCount = 2;
            this.仪表盘1.SecondTickLegth = 0.1F;
            graphicsPath14.FillMode = System.Drawing.Drawing2D.FillMode.Alternate;
            this.仪表盘1.SecondTickPath = graphicsPath14;
            this.仪表盘1.SecondTickWidth = 2;
            this.仪表盘1.Size = new System.Drawing.Size(300, 300);
            this.仪表盘1.StartAngle = 20F;
            this.仪表盘1.SweepAngle = 140F;
            this.仪表盘1.TabIndex = 6;
            this.仪表盘1.Text = "仪表盘1";
            this.仪表盘1.ThirdTickColor = System.Drawing.Color.Black;
            this.仪表盘1.ThirdTickCount = 5;
            this.仪表盘1.ThirdTickLegth = 0.05F;
            graphicsPath15.FillMode = System.Drawing.Drawing2D.FillMode.Alternate;
            this.仪表盘1.ThirdTickPath = graphicsPath15;
            this.仪表盘1.ThirdTickWidth = 1;
            this.仪表盘1.TickNumberDecimalPointCount = 1;
            this.仪表盘1.TickNumberDirection = true;
            this.仪表盘1.TickNumberFont = new System.Drawing.Font("宋体", 9F);
            this.仪表盘1.TickNumberSize = 0.65F;
            this.仪表盘1.TickNumColor = System.Drawing.Color.Black;
            this.仪表盘1.TickRingAlarmColor = System.Drawing.Color.PaleGoldenrod;
            this.仪表盘1.TickRingColor = System.Drawing.Color.Black;
            this.仪表盘1.TickRingErrorColor = System.Drawing.Color.Sienna;
            graphicsPath16.FillMode = System.Drawing.Drawing2D.FillMode.Alternate;
            this.仪表盘1.TickRingPath = graphicsPath16;
            this.仪表盘1.TickRingShow = true;
            this.仪表盘1.TickRingWidth = 2;
            this.仪表盘1.TickSize = 0.95F;
            this.仪表盘1.Unit = "";
            this.仪表盘1.UnitPerDegrees = 1.4F;
            this.仪表盘1.Value = 20F;
            this.仪表盘1.ValueFont = new System.Drawing.Font("宋体", 12F);
            this.仪表盘1.ValuePositionOffset = ((System.Drawing.PointF)(resources.GetObject("仪表盘1.ValuePositionOffset")));
            // 
            // 仪表盘5
            // 
            this.仪表盘5.CurrentDecimalPlaces = ((byte)(2));
            this.仪表盘5.CurrentValueColor = System.Drawing.Color.Red;
            this.仪表盘5.Description = "仪表";
            this.仪表盘5.DescriptionColor = System.Drawing.Color.Black;
            this.仪表盘5.DescriptionFont = new System.Drawing.Font("宋体", 9F);
            this.仪表盘5.DescriptionPoint = ((System.Drawing.PointF)(resources.GetObject("仪表盘5.DescriptionPoint")));
            this.仪表盘5.HightAlarm = 90F;
            this.仪表盘5.Location = new System.Drawing.Point(28, 66);
            this.仪表盘5.LowAlarm = 60F;
            this.仪表盘5.MainTickColor = System.Drawing.Color.Blue;
            this.仪表盘5.MainTickCount = 10;
            this.仪表盘5.MainTickLegth = 0.15F;
            graphicsPath17.FillMode = System.Drawing.Drawing2D.FillMode.Alternate;
            this.仪表盘5.MainTickPath = graphicsPath17;
            this.仪表盘5.MainTickWidth = 3;
            this.仪表盘5.Max = 100F;
            this.仪表盘5.MeterPosition = ((System.Drawing.PointF)(resources.GetObject("仪表盘5.MeterPosition")));
            this.仪表盘5.MeterRadius = 1F;
            this.仪表盘5.Min = 0F;
            this.仪表盘5.Name = "仪表盘5";
            this.仪表盘5.NeedleColor = System.Drawing.Color.Green;
            this.仪表盘5.NeedleHight = 0.05F;
            this.仪表盘5.NeedleStytle = AutoUI.EnumStytle.Stytle2;
            this.仪表盘5.NeedleWidth = 0.5F;
            this.仪表盘5.Point_Center = ((System.Drawing.PointF)(resources.GetObject("仪表盘5.Point_Center")));
            this.仪表盘5.Radius = 220F;
            this.仪表盘5.SecondTickColor = System.Drawing.Color.BlanchedAlmond;
            this.仪表盘5.SecondTickCount = 4;
            this.仪表盘5.SecondTickLegth = 0.1F;
            graphicsPath18.FillMode = System.Drawing.Drawing2D.FillMode.Alternate;
            this.仪表盘5.SecondTickPath = graphicsPath18;
            this.仪表盘5.SecondTickWidth = 2;
            this.仪表盘5.Size = new System.Drawing.Size(444, 440);
            this.仪表盘5.StartAngle = 330F;
            this.仪表盘5.SweepAngle = 240F;
            this.仪表盘5.TabIndex = 7;
            this.仪表盘5.Text = "仪表盘5";
            this.仪表盘5.ThirdTickColor = System.Drawing.Color.Black;
            this.仪表盘5.ThirdTickCount = 8;
            this.仪表盘5.ThirdTickLegth = 0.05F;
            graphicsPath19.FillMode = System.Drawing.Drawing2D.FillMode.Alternate;
            this.仪表盘5.ThirdTickPath = graphicsPath19;
            this.仪表盘5.ThirdTickWidth = 1;
            this.仪表盘5.TickNumberDecimalPointCount = 1;
            this.仪表盘5.TickNumberDirection = false;
            this.仪表盘5.TickNumberFont = new System.Drawing.Font("宋体", 9F);
            this.仪表盘5.TickNumberSize = 0.65F;
            this.仪表盘5.TickNumColor = System.Drawing.Color.Black;
            this.仪表盘5.TickRingAlarmColor = System.Drawing.Color.OrangeRed;
            this.仪表盘5.TickRingColor = System.Drawing.Color.Black;
            this.仪表盘5.TickRingErrorColor = System.Drawing.Color.RosyBrown;
            graphicsPath20.FillMode = System.Drawing.Drawing2D.FillMode.Alternate;
            this.仪表盘5.TickRingPath = graphicsPath20;
            this.仪表盘5.TickRingShow = true;
            this.仪表盘5.TickRingWidth = 2;
            this.仪表盘5.TickSize = 0.95F;
            this.仪表盘5.Unit = "";
            this.仪表盘5.UnitPerDegrees = 2.4F;
            this.仪表盘5.Value = 20F;
            this.仪表盘5.ValueFont = new System.Drawing.Font("宋体", 12F);
            this.仪表盘5.ValuePositionOffset = ((System.Drawing.PointF)(resources.GetObject("仪表盘5.ValuePositionOffset")));
            // 
            // 仪表盘
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1171, 564);
            this.Controls.Add(this.仪表盘5);
            this.Controls.Add(this.仪表盘1);
            this.Controls.Add(this.仪表盘3);
            this.Controls.Add(this.仪表盘2);
            this.Controls.Add(this.仪表盘4);
            this.Name = "仪表盘";
            this.Text = "仪表盘";
            this.ResumeLayout(false);

        }

        #endregion

        private AutoUI.仪表盘 仪表盘4;
        private AutoUI.仪表盘 仪表盘2;
        private AutoUI.仪表盘 仪表盘3;
        private AutoUI.仪表盘 仪表盘1;
        private AutoUI.仪表盘 仪表盘5;
    }
}