﻿namespace DemoFramework
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            System.Drawing.Drawing2D.GraphicsPath graphicsPath1 = new System.Drawing.Drawing2D.GraphicsPath();
            System.Drawing.Drawing2D.GraphicsPath graphicsPath2 = new System.Drawing.Drawing2D.GraphicsPath();
            System.Drawing.Drawing2D.GraphicsPath graphicsPath3 = new System.Drawing.Drawing2D.GraphicsPath();
            System.Drawing.Drawing2D.GraphicsPath graphicsPath4 = new System.Drawing.Drawing2D.GraphicsPath();
            this.仪表盘1 = new AutoUI.仪表盘();
            this.SuspendLayout();
            // 
            // 仪表盘1
            // 
            this.仪表盘1.CurrentDecimalPlaces = ((byte)(2));
            this.仪表盘1.CurrentValueColor = System.Drawing.Color.Red;
            this.仪表盘1.Description = "仪表";
            this.仪表盘1.DescriptionColor = System.Drawing.Color.Black;
            this.仪表盘1.DescriptionFont = new System.Drawing.Font("宋体", 9F);
            this.仪表盘1.DescriptionPoint = ((System.Drawing.PointF)(resources.GetObject("仪表盘1.DescriptionPoint")));
            this.仪表盘1.HightAlarm = 90F;
            this.仪表盘1.Location = new System.Drawing.Point(325, 10);
            this.仪表盘1.LowAlarm = 60F;
            this.仪表盘1.MainTickColor = System.Drawing.Color.Black;
            this.仪表盘1.MainTickCount = 10;
            this.仪表盘1.MainTickLegth = 0.15F;
            graphicsPath1.FillMode = System.Drawing.Drawing2D.FillMode.Alternate;
            this.仪表盘1.MainTickPath = graphicsPath1;
            this.仪表盘1.MainTickWidth = 3;
            this.仪表盘1.Max = 100F;
            this.仪表盘1.MeterPosition = ((System.Drawing.PointF)(resources.GetObject("仪表盘1.MeterPosition")));
            this.仪表盘1.MeterRadius = 1F;
            this.仪表盘1.Min = 0F;
            this.仪表盘1.Name = "仪表盘1";
            this.仪表盘1.NeedleColor = System.Drawing.Color.Green;
            this.仪表盘1.NeedleHight = 0.05F;
            this.仪表盘1.NeedleStytle = AutoUI.EnumStytle.Stytle2;
            this.仪表盘1.NeedleWidth = 0.5F;
            this.仪表盘1.Point_Center = ((System.Drawing.PointF)(resources.GetObject("仪表盘1.Point_Center")));
            this.仪表盘1.Radius = 150F;
            this.仪表盘1.SecondTickColor = System.Drawing.Color.Black;
            this.仪表盘1.SecondTickCount = 2;
            this.仪表盘1.SecondTickLegth = 0.1F;
            graphicsPath2.FillMode = System.Drawing.Drawing2D.FillMode.Alternate;
            this.仪表盘1.SecondTickPath = graphicsPath2;
            this.仪表盘1.SecondTickWidth = 2;
            this.仪表盘1.Size = new System.Drawing.Size(300, 300);
            this.仪表盘1.StartAngle = 330F;
            this.仪表盘1.SweepAngle = 240F;
            this.仪表盘1.TabIndex = 0;
            this.仪表盘1.Text = "仪表盘1";
            this.仪表盘1.ThirdTickColor = System.Drawing.Color.Black;
            this.仪表盘1.ThirdTickCount = 5;
            this.仪表盘1.ThirdTickLegth = 0.05F;
            graphicsPath3.FillMode = System.Drawing.Drawing2D.FillMode.Alternate;
            this.仪表盘1.ThirdTickPath = graphicsPath3;
            this.仪表盘1.ThirdTickWidth = 1;
            this.仪表盘1.TickNumberDecimalPointCount = 1;
            this.仪表盘1.TickNumberDirection = false;
            this.仪表盘1.TickNumberFont = new System.Drawing.Font("宋体", 9F);
            this.仪表盘1.TickNumberSize = 0.65F;
            this.仪表盘1.TickNumColor = System.Drawing.Color.Black;
            this.仪表盘1.TickRingAlarmColor = System.Drawing.Color.Orange;
            this.仪表盘1.TickRingColor = System.Drawing.Color.Black;
            this.仪表盘1.TickRingErrorColor = System.Drawing.Color.Red;
            graphicsPath4.FillMode = System.Drawing.Drawing2D.FillMode.Alternate;
            this.仪表盘1.TickRingPath = graphicsPath4;
            this.仪表盘1.TickRingShow = true;
            this.仪表盘1.TickRingWidth = 2;
            this.仪表盘1.TickSize = 0.95F;
            this.仪表盘1.Unit = "";
            this.仪表盘1.UnitPerDegrees = 2.4F;
            this.仪表盘1.Value = 20F;
            this.仪表盘1.ValueFont = new System.Drawing.Font("宋体", 12F);
            this.仪表盘1.ValuePositionOffset = ((System.Drawing.PointF)(resources.GetObject("仪表盘1.ValuePositionOffset")));
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1113, 587);
            this.Controls.Add(this.仪表盘1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private AutoUI.仪表盘 仪表盘1;
    }
}