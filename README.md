# AutoUI

#### 介绍
1. 这是一款特别的Winform控件库,一切才刚开始...
2. 作为winform钉子户,都是情怀!诚邀能一起做下去的(大佬,小佬,菜鸟都可以)!
3. 需要什么样子的控件可以说,做的出来我都做,Chart控件算了,国外有很好的开源的
4. 属性通过特性的方式改为了中文,方便普通使用(为我点赞,我是多么的好心)
![输入图片说明](DemoFramework/Properties/%E5%B1%9E%E6%80%A7%E4%BB%8B%E7%BB%8D.png)
- 感兴趣加QQ交流群：[772759046](https://qm.qq.com/cgi-bin/qm/qr?k=VdbVfVms_LpzgS_ucvtgydID4PO3I_My&jump_from=webapi) 
- 项目地址:https://gitee.com/ji-yunliu/AutoUI
#### 安装教程

1.  Nuget上直接搜索AutoUI就好了![输入图片说明](DemoFramework/Properties/nuget_autoui.png)
2.  码云直接下载源码,自己编译一样的
3.  ......

#### 使用说明
1. 界面都采用中文属性,后期会完善说明文字!后期出视频教程!
2.支持老系统,net3.5以上就好,推荐4.8最好!.net6没测试,应该是没有问题的.

##### 高级流动条:
- 亮点:路径随意通过鼠标绘制,可以通过鼠标拖动修改,也可以属性更改坐标值!且能适应自动缩放,通过属性控制是否流动,以及流动速度...
- 精髓在于根据鼠标生成路径的算法,你还嫩继续优化,我暂时不想折腾这个控件了(花的时间比较多了在这个上面)
![流动条](DemoFramework/Properties/%E6%B5%81%E5%8A%A8%E6%9D%A1%E6%BC%94%E7%A4%BA.gif)
##### 仪表盘:
- 只实现了基本的功能,自我感觉还需要改进很多!比如目前指针只有两种,可以增加N种....
- 亮点:1.提供设计时功能,能够鼠标拖动控制文字等绘制位子;2.随意自定义绘制附加装饰
- 欢迎提意见/建议.
![仪表盘](DemoFramework/Properties/%E4%BB%AA%E8%A1%A8%E7%9B%98%E6%BC%94%E7%A4%BA.gif)
##### 按钮扩展
- 通过改变按钮的显示区域,从而改变显示形状!理论任何形状都可以绘制出来
- 亮点:不仅仅看上去形状变了,实际上他也变了,因为Region变了,当我们点击形状外区域,并不会触发Click事件!
- ![输入图片说明](DemoFramework/Properties/%E6%89%A9%E5%B1%95%E6%8C%89%E9%92%AE%E6%BC%94%E7%A4%BA.gif)

##### 直线
##### 图像框
##### 电动机
##### 电风扇
##### 灯泡
#### 参与贡献

1.  重在参与,欢迎大佬,小佬,菜鸟
2. 希望这里出现你的头像![头像](https://tse4-mm.cn.bing.net/th/id/OIP-C.p0yUjLoinUFwzMttFCIxOwHaE4?w=100&h=100&c=7&r=0&o=5&dpr=1.25&pid=1.7)
![头像](https://tse4-mm.cn.bing.net/th/id/OIP-C.p0yUjLoinUFwzMttFCIxOwHaE4?w=100&h=100&c=7&r=0&o=5&dpr=1.25&pid=1.7)
![头像](https://tse4-mm.cn.bing.net/th/id/OIP-C.p0yUjLoinUFwzMttFCIxOwHaE4?w=100&h=100&c=7&r=0&o=5&dpr=1.25&pid=1.7)
![头像](https://tse4-mm.cn.bing.net/th/id/OIP-C.p0yUjLoinUFwzMttFCIxOwHaE4?w=100&h=100&c=7&r=0&o=5&dpr=1.25&pid=1.7)
![头像](https://tse4-mm.cn.bing.net/th/id/OIP-C.p0yUjLoinUFwzMttFCIxOwHaE4?w=100&h=100&c=7&r=0&o=5&dpr=1.25&pid=1.7)



#### 特技


