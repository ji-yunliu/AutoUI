﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AutoUI
{
    [Description("直线")]
    public partial class 直线 : Control
    {
        public 直线()
        {
            InitializeComponent();
        }

        private PointF rstart;
        private PointF rend;
        private PointF start = new PointF(1, 0.5f);

        [TypeConverter(typeof(PointFConvertor))]
        public PointF Start
        {
            get { return start; }
            set
            {
                start = value; init();
                Invalidate();
            }
        }

        private PointF end = new PointF(0, 0.5f);

        [TypeConverter(typeof(PointFConvertor))]
        public PointF End
        {
            get { return end; }
            set
            {
                end = value; init();
                Invalidate();
            }
        }

        private Color lineColor = Color.Black;

        public Color LineColor
        {
            get { return lineColor; }
            set
            {
                lineColor = value;
                p.Color = lineColor;
                Invalidate();
            }
        }

        private float lineWidth = 1;

        public float LineWidth
        {
            get { return lineWidth; }
            set
            {
                lineWidth = value; p.Width = lineWidth;
                init();
                Invalidate();
            }
        }

        private EnumDirection direction;

        public EnumDirection Direction
        {
            get { return direction; }
            set
            {
                direction = value;
                switch (direction)
                {
                    case EnumDirection.Vertical:
                        start = new PointF(0.5f, 0);
                        end = new PointF(0.5f, 1);
                        break;

                    case EnumDirection.Horizontal:
                        start = new PointF(0, 0.5f);
                        end = new PointF(1, 0.5f);
                        break;

                    case EnumDirection.Custom:
                        break;

                    default:
                        break;
                }
                init();
                Invalidate();
            }
        }

        private void init()
        {
            rstart = new PointF(Width * start.X, Height * start.Y);
            rend = new PointF(Width * end.X, Height * end.Y);
            AutoLine l = new AutoLine(start, end);
            CircleF c1 = new CircleF(rstart, lineWidth / 2.0f + 10);
            CircleF c2 = new CircleF(rend, lineWidth / 2.0f + 10);
            var p1 = c1.PointOnPath(l.Angel + 90);
            var p2 = c1.PointOnPath(l.Angel - 90);
            var p3 = c2.PointOnPath(l.Angel + 90);
            var p4 = c2.PointOnPath(l.Angel - 90);
            gp = new GraphicsPath();
            gp.AddLines(new PointF[] { p1, p3, p4, p2 });
            this.Region = new Region(gp);
        }

        protected override void OnSizeChanged(EventArgs e)
        {
            var x = this.Width;
            rstart = new PointF(Width * start.X, Height * start.Y);
            rend = new PointF(Width * end.X, Height * end.Y);
            init();
            base.OnSizeChanged(e);
        }

        protected override void SetBoundsCore(int x, int y, int width, int height, BoundsSpecified specified)
        {
            base.SetBoundsCore(x, y, width, height, specified);
        }

        private GraphicsPath gp = new GraphicsPath();

        protected override void InitLayout()
        {
            base.InitLayout();

            init();
        }

        private Pen p = new Pen(Color.Black);

        protected override void OnPaint(PaintEventArgs pe)
        {
            base.OnPaint(pe);
            var g = pe.Graphics;
            g.SmoothingMode = SmoothingMode.HighQuality;
            g.DrawLine(p, rstart, rend);
        }
    }
}