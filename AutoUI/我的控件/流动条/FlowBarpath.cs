﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoUI
{
    public class FlowBarpath
    {
        // DrawLinepath, DrawLinepath, FillLinepath, FillPathpath, AutoLine
        public DrawLinepath Item1 { get; set; }

        public DrawLinepath Item2 { get; set; }
        public FillLinepath Item3 { get; set; }
        public FillPathpath Item4 { get; set; }
        public AutoLine Item5 { get; set; }
    }
}