﻿using System;
using System.Collections.Generic;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;

namespace AutoUI
{
    /// <summary>
    /// 一个路径和他的填充方式
    /// </summary>
    public class FillPathpath
    {
        public GraphicsPath Path { get; set; }
        public PathGradientBrush PathBrush { get; set; }
    }
}