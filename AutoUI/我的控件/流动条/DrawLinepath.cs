﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;

namespace AutoUI
{
    /// <summary>
    /// 一个路径和绘制这个路径的方式(颜色)
    /// </summary>
    public class DrawLinepath
    {
        public GraphicsPath Path { get; set; }
        public Pen Pen { get; set; }
    }
}