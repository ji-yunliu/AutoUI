﻿using System;
using System.Collections.Generic;
using System.Drawing.Design;
using System.Linq;
using System.Text;

namespace AutoUI
{
    [Serializable]
    public class ToolBoxItemFlowBar : ToolboxItem
    {
        public ToolBoxItemFlowBar()
        {
            DisplayName = "流动条";
            TypeName = "AutoUI.高级流动条";
        }
    }
}