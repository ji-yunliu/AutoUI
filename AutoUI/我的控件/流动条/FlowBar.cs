﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;

namespace AutoUI
{
    /// <summary>
    /// 表示一条流动条:流动条的点集合(比例)及颜色等
    /// </summary>
    public class FlowBar : IDisposable
    {
        /// <summary>
        ///
        /// </summary>
        public FlowBar()
        {
            Points = new List<PointF>();
            _FlowPen = new Pen(Color.Red, 1);
            _ColorBlend = new ColorBlend();
        }

        public List<FillLinepath> FillLinepaths = new List<FillLinepath>();
        public List<FillPathpath> FillPathpaths = new List<FillPathpath>();
        public List<DrawLinepath> DrawLinepaths = new List<DrawLinepath>();

        /// <summary>
        /// 最后一条直线
        /// </summary>
        public AutoLine LastLine;

        /// <summary>
        /// 绘制流动液体的笔
        /// </summary>
        public Pen _FlowPen = new Pen(Color.Red, 1);

        /// <summary>
        /// 渐变色
        /// </summary>
        public ColorBlend _ColorBlend = new ColorBlend();

        /// <summary>
        /// 流动体偏移
        /// </summary>
        public byte _offset = 0;

        /// <summary>
        /// 流动体偏移
        /// </summary>
        public int time = 0;

        #region Properties

        /// <summary>
        /// 是否流动
        /// </summary>
        [Category("3.参数设置"), Description("是否流动"), DisplayName("是否流动")]
        public EnumRotateDirection Active { get; set; }

        /// <summary>
        /// 流动方向:顺时针还是逆时针"
        /// </summary>
        [Category("3.参数设置"), Description("流动方向:顺时针还是逆时针"), DisplayName("流动方向")]
        public bool ColockWise { get; set; }

        /// <summary>
        /// 渐变结束颜色
        /// </summary>
        [Category("1.颜色设置"), Description("渐变结束颜色"), DisplayName("渐变结束颜色")]
        public Color EndColor { get; set; } = Color.White;

        /// <summary>
        /// 流动条颜色
        /// </summary>
        [Category("1.颜色设置"), Description("流动条液体颜色"), DisplayName("流动条液体颜色")]
        public Color FlowFluidColor { get; set; } = Color.Red;

        /// <summary>
        /// 流动条液体高度:绝对像素表示
        /// </summary>
        [Category("2.形状位置大小设置"), Description("流动条液体高度:绝对像素表示"), DisplayName("流动液体高度")]
        public float FlowFluidHeight { get; set; } = 15f;

        /// <summary>
        /// 流动条高度:绝对像素表示
        /// </summary>
        [Category("2.形状位置大小设置"), Description("流动条高度:绝对像素表示"), DisplayName("流动条高度")]
        public float FlowBarHeight { get; set; } = 30f;

        /// <summary>
        /// 流动液体长度
        /// </summary>
        [Category("2.形状位置大小设置"), Description("流动液体长度"), DisplayName("流动液体长度")]
        public int FolwLength { get; set; } = 40;

        /// <summary>
        /// 流动液体间隙长度
        /// </summary>
        [Category("2.形状位置大小设置"), Description("流动液体间隙长度"), DisplayName("流动液体间隙长度"), DefaultValue(10)]
        public int GapLength { get; set; } = 20;

        /// <summary>
        /// 统一的比例点:设计时候复制给这个属性的,控制流动条的路径的
        /// </summary>
        [Category("所有的数据点"), Description("路径点:通过设计时候选择")]
        public List<PointF> Points { get; set; }

        /// <summary>
        /// 根据Points转化之后的点
        /// </summary>
        public List<PointF> RealPoints = new List<PointF>();

        /// <summary>
        /// 流动速度
        /// </summary>
        [Category("3.参数设置"), Description("流动速度"), DefaultValue(100), DisplayName("流动速度")]
        public int Speed { get; set; } = 100;

        /// <summary>
        /// 渐变开始颜色
        /// </summary>
        [Category("1.颜色设置"), Description("渐变开始颜色"), DisplayName("渐变开始颜色")]
        public Color StartColor { get; set; } = Color.Cyan;

        public void Dispose()
        {
            if (_FlowPen != null)
            {
                _FlowPen.Dispose();
            }
        }

        #endregion Properties
    }
}