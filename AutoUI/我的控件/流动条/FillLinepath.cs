﻿using System;
using System.Collections.Generic;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;

namespace AutoUI
{
    /// <summary>
    /// 一个矩形和他的填充方式
    /// </summary>
    public class FillLinepath
    {
        public GraphicsPath Path { get; set; }
        public LinearGradientBrush PathBrush { get; set; }
    }
}