﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AutoUI
{
    [Designer(typeof(CustomerControlDesigner)), DefaultProperty("AutoShapes_")]
    [Description("所有功能的集合")]
    public partial class 自定义组合控件 : Control
    {
        #region 字段

        private List<AutoShape> autoShapes = new List<AutoShape>();

        #endregion 字段

        #region Constructors

        public 自定义组合控件()
        {
            InitializeComponent();

            SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.OptimizedDoubleBuffer | ControlStyles.ResizeRedraw | ControlStyles.SupportsTransparentBackColor, true);
        }

        #endregion Constructors

        #region 属性

        /// <summary>
        /// 所有形状的集合
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [Localizable(true)]
        [MergableProperty(false)]
        [DisplayName("图形管理"), Category("1.主要参数")]
        public List<AutoShape> AutoShapes_
        {
            get => autoShapes; set
            {
                autoShapes = value; init(); Invalidate();
            }
        }

        [DisplayName("正在编辑的图形索引"), Category("最不重要参数")]
        public int IndexDesigner { get; set; } = -1;

        #endregion 属性

        #region Methods

        public void init()
        {
            if (Width > 0 && Height > 0)
            {
                foreach (var AutoShape in AutoShapes_)
                {
                    AutoShape.CreatePath(Width, Height);
                }
            }
            Invalidate();
        }

        protected override void InitLayout()
        {
            base.InitLayout();
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            var g = e.Graphics;
            g.SmoothingMode = SmoothingMode.AntiAlias;
            foreach (var AutoShape in AutoShapes_)
            {
                if (AutoShape.Path != null && AutoShape.Brush != null)
                {
                    switch (AutoShape.DrawOrFill)
                    {
                        case EnumDrawOrFill.OnlyDraw:
                            g.DrawPath(AutoShape.Pen, AutoShape.Path);

                            break;

                        case EnumDrawOrFill.OnlyFill:
                            g.FillPath(AutoShape.Brush, AutoShape.Path);
                            break;

                        case EnumDrawOrFill.DrawAndFill:
                            g.FillPath(AutoShape.Brush, AutoShape.Path);
                            g.DrawPath(AutoShape.Pen, AutoShape.Path);

                            break;

                        default:
                            break;
                    }
                }
            }
        }

        protected override void OnSizeChanged(EventArgs e)
        {
            base.OnSizeChanged(e);
            init();
        }

        #endregion Methods
    }
}