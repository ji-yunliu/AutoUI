﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Forms.Design;

using System.Windows.Forms.Design.Behavior;

namespace AutoUI
{
    /// <summary>
    /// 自定义组合控件设计器
    /// </summary>
    public class CustomerControlDesigner : ControlDesigner
    {
        #region Fields

        private DesignerVerbCollection _verbs;

        #endregion Fields

        #region Properties

        public override SelectionRules SelectionRules
        {
            get
            {
                SelectionRules sr = base.SelectionRules;
                //if (((test)Control).DesignerMove)
                //{
                //    sr |= SelectionRules.Moveable;
                //}
                //else
                //{
                //    sr &= ~SelectionRules.Moveable;
                //}
                return sr;
            }
        }

        public override IList SnapLines
        {
            get
            {
                IList lines = new ArrayList();
                //左对齐线
                SnapLine left1 = new SnapLine(SnapLineType.Left, 1);
                SnapLine left2 = new SnapLine(SnapLineType.Right, 1);
                //右对齐线
                SnapLine left3 = new SnapLine(SnapLineType.Left, Control.Width - 1);
                SnapLine left4 = new SnapLine(SnapLineType.Right, Control.Width - 1);

                SnapLine v1ine1 = new SnapLine(SnapLineType.Top, 1);
                SnapLine v1ine2 = new SnapLine(SnapLineType.Bottom, 1);

                SnapLine v1ine3 = new SnapLine(SnapLineType.Top, Control.Height - 1);
                SnapLine v1ine4 = new SnapLine(SnapLineType.Bottom, Control.Height - 1);

                SnapLine l1 = new SnapLine(SnapLineType.Left, 0);
                SnapLine l2 = new SnapLine(SnapLineType.Right, 0);
                //右对齐线
                SnapLine l3 = new SnapLine(SnapLineType.Left, Control.Width - 0);
                SnapLine l4 = new SnapLine(SnapLineType.Right, Control.Width - 0);

                SnapLine l5 = new SnapLine(SnapLineType.Top, 0);
                SnapLine l6 = new SnapLine(SnapLineType.Bottom, 0);

                SnapLine l7 = new SnapLine(SnapLineType.Top, Control.Height - 0);
                SnapLine l8 = new SnapLine(SnapLineType.Bottom, Control.Height - 0);
                lines.Add(left1);
                lines.Add(left2);
                lines.Add(left3);
                lines.Add(left4);
                lines.Add(v1ine1);
                lines.Add(v1ine2);
                lines.Add(v1ine3);
                lines.Add(v1ine4);

                lines.Add(l1);
                lines.Add(l2);
                lines.Add(l3);
                lines.Add(l4);
                lines.Add(l5);
                lines.Add(l6);
                lines.Add(l7);
                lines.Add(l8);

                return lines;
            }
        }

        /// <summary>
        /// 添加右键菜单
        /// </summary>
        public override DesignerVerbCollection Verbs
        {
            get
            {
                if (_verbs == null)
                {
                    _verbs = new DesignerVerbCollection();
                    _verbs.Add(new DesignerVerb("开始设计", new EventHandler(OnActivate)));
                    _verbs.Add(new DesignerVerb("删除最后一个点", new EventHandler(ChangeOrientation)));
                }
                return _verbs;
            }
        }

        #endregion Properties

        #region Methods

        protected override void OnMouseDragBegin(int x, int y)
        {
            base.OnMouseDragBegin(x, y);
        }

        protected override void OnMouseDragMove(int x, int y)
        {
            自定义组合控件 ab = (自定义组合控件)this.Control;
            System.Drawing.Point point = ab.PointToClient(Control.MousePosition);
            if (ab.IndexDesigner != -1)
            {
                //
                var xx = (point.X) / (float)ab.Width;
                var yy = (point.Y) / (float)ab.Height;
                // GetPropertyByName($"AutoShapes[{ab.IndexDesigner}].Position").SetValue(ab, new PointF(xx, yy));
                ab.AutoShapes_[ab.IndexDesigner].Position = new PointF(xx, yy);
                ab.init();
                ab.Invalidate();
            }
            else
            {
                base.OnMouseDragMove(x, y);
            }
        }

        protected override void OnMouseEnter()
        {
            designering = true;
            自定义组合控件 ab = (自定义组合控件)this.Control;
            ab.init();
            ab.Invalidate();
            base.OnMouseEnter();
        }

        protected override void OnMouseLeave()
        {
            designering = false;
            自定义组合控件 ab = (自定义组合控件)this.Control;
            ab.init();
            ab.Invalidate();
            base.OnMouseLeave();
        }

        private bool designering;

        protected override void OnPaintAdornments(PaintEventArgs pe)
        {
            自定义组合控件 ab = (自定义组合控件)this.Control;
            System.Drawing.Point point = ab.PointToClient(Control.MousePosition);
            // var pscale = new PointF(ab.Width * ab.DescriptionPoint.X, ab.Height * ab.DescriptionPoint.Y);
            var g = pe.Graphics;
            if (designering)
            {
                for (int i = 0; i < ab.AutoShapes_.Count; i++)
                {
                    AutoShape item = ab.AutoShapes_[i];
                    CircleF c = new CircleF(new PointF(item.Position.X * ab.Width, item.Position.Y * ab.Height), 30);
                    g.DrawArc(Pens.LightBlue, c, 0, 360);
                    //if (item.Brush is PathGradientBrush pg)
                    //{
                    //    var p = item.GradientCenterPoint;
                    //    CircleF c2 = new CircleF(p, 30);
                    //    g.DrawArc(Pens.LightBlue, c2, 0, 360);
                    //}
                }
            }
        }

        /// <summary>
        /// 处理 Windows 消息，并可以选择将其路由到控件。
        /// </summary>
        /// <param name="m"></param>
        protected override void WndProc(ref Message m)
        {
            if (m.Msg == Win32.WM_LBUTTONDOWN)
            {
                this.OnMouseLeftDown();
            }
            else if (m.Msg == Win32.WM_LBUTTONUP)
            {
                this.OnMouseLeftUp();
            }
            else if (m.Msg == Win32.WM_RBUTTONUP)
            {
                this.OnMouseRightUp();
            }
            else if (m.Msg == Win32.WM_MOUSEMOVE)
            {
                this.OnMouseMove();
            }

            base.WndProc(ref m);
        }

        private void ChangeOrientation(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 获取正在设计的控件实例的某一个属性
        /// </summary>
        /// <param name="propName"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentException"></exception>
        private PropertyDescriptor GetPropertyByName(String propName)
        {
            PropertyDescriptor prop;
            prop = TypeDescriptor.GetProperties(Control)[propName];
            if (null == prop)
                throw new ArgumentException("组件未定义该属性。", propName);
            else
                return prop;
        }

        private void OnActivate(object sender, EventArgs e)
        {
            自定义组合控件 ab = (自定义组合控件)this.Control;
        }

        private void OnMouseLeftDown()
        {
            自定义组合控件 ab = (自定义组合控件)this.Control;
            System.Drawing.Point point = ab.PointToClient(Control.MousePosition);
            // var pscale = new PointF(ab.Width * ab.DescriptionPoint.X, ab.Height * ab.DescriptionPoint.Y);

            for (int i = 0; i < ab.AutoShapes_.Count; i++)
            {
                AutoShape item = ab.AutoShapes_[i];
                var realpoint = new PointF(item.Position.X * ab.Width, item.Position.Y * ab.Height);
                if (realpoint.CreateCircleF(30).Contains(point))
                {
                    ab.IndexDesigner = i;
                    return;
                }
            }
        }

        private void OnMouseLeftUp()
        {
            自定义组合控件 ab = (自定义组合控件)this.Control;
            GetPropertyByName("IndexDesigner").SetValue(ab, -1);
        }

        private void OnMouseMove()
        {
            自定义组合控件 ab = (自定义组合控件)this.Control;
            System.Drawing.Point point = ab.PointToClient(Control.MousePosition);
        }

        private void OnMouseRightUp()
        {
        }

        #endregion Methods
    }
}