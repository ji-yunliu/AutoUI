using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.ComponentModel;
using System.Runtime.InteropServices;

namespace ColorPane
{
    /// <summary>
    /// 封装颜色拾取器控件
    /// </summary>
    [ToolboxItem(false)]
    public partial class ColorDrawer : Control
    {
        /// <summary>
        /// RGB颜色
        /// </summary>
        private Color _color = Color.DarkGreen;

        /// <summary>
        /// HSB颜色
        /// </summary>
        private ColorSpace.HSB _hsb;

        /// <summary>
        /// Lab颜色
        /// </summary>
        private ColorSpace.LAB _lab;

        /// <summary>
        /// 绘制当前使用的颜色映射表使用的画刷
        /// </summary>
        private PathGradientBrush _colortablebrush;

        /// <summary>
        /// 背景画刷
        /// </summary>
        private TextureBrush _backTexture;

        /// <summary>
        /// 表示当前使用的颜色映射表使用的路径
        /// </summary>
        private GraphicsPath _colortablepath;

        /// <summary>
        /// ColorDrawer使用的色谱绘制类型
        /// </summary>
        private ColorTableType _colorTableType = ColorTableType.Blue;

        /// <summary>
        /// 指定滑标所在的位置
        /// </summary>
        private Point _sliderlocation;

        #region 事件

        /// <summary>
        /// 颜色(RGB,HSB,Lab)改变
        /// </summary>
        public event EventHandler ColorChanged;

        /// <summary>
        /// 滑标位置发生改变
        /// </summary>
        public event EventHandler SliderScroll;

        #endregion 事件

        #region 属性

        [Description("RGB颜色"), Category("颜色空间")]
        public Color Color
        {
            get { return _color; }
            set
            {
                if (_color != value)
                {
                    _color = value;
                    _hsb = ColorSpace.RGB2HSB(_color);
                    _lab = ColorSpace.RGB2LAB(_color);
                    OnColorChanged(EventArgs.Empty);
                }
            }
        }

        [Description("HSB颜色"), Category("颜色空间")]
        public ColorSpace.HSB HSB
        {
            get { return _hsb; }
            set
            {
                if (_hsb != value)
                {
                    _hsb = value;
                    _color = _hsb.ToRGB();
                    _lab = ColorSpace.RGB2LAB(_color);
                    OnColorChanged(EventArgs.Empty);
                }
            }
        }

        [Description("Lab颜色"), Category("颜色空间")]
        public ColorSpace.LAB Lab
        {
            get { return _lab; }
            set
            {
                if (_lab != value)
                {
                    _lab = value;
                    _color = _lab.ToRGB();
                    _hsb = ColorSpace.HSB.FromRGB(_color);
                    OnColorChanged(EventArgs.Empty);
                }
            }
        }

        [Description("色谱类型"), Category("颜色空间")]
        public ColorTableType ColorTableType
        {
            get { return _colorTableType; }
            set
            {
                if (_colorTableType != value)
                {
                    _colorTableType = value;
                    _sliderlocation = GetPosition();
                    Invalidate();
                }
            }
        }

        #endregion 属性

        public ColorDrawer() : base()
        {
            //设置控件的样式，使其支持透明背景并启用双缓存绘图
            SetStyle(ControlStyles.Selectable | ControlStyles.OptimizedDoubleBuffer | ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint
                | ControlStyles.SupportsTransparentBackColor | ControlStyles.ResizeRedraw, true);
            _hsb = ColorSpace.RGB2HSB(_color);
            _lab = ColorSpace.RGB2LAB(_color);
            _sliderlocation = GetPosition();
        }

        protected override void OnParentChanged(EventArgs e)
        {
            base.OnParentChanged(e);
            if (!DesignMode)
                Cursor = CreateCursor();
            else
            {
                Cursor = Cursors.Default;
                //MessageBox.Show("Seji");
            }
        }

        /// <summary>
        /// 释放使用的资源
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            if (disposing)
            {
                if (_colortablebrush != null) _colortablebrush.Dispose();
                if (_backTexture != null) _backTexture.Dispose();
                if (_colortablepath != null) _colortablepath.Dispose();
                GC.Collect();
            }
        }

        /// <summary>
        /// 控件的大小改变
        /// </summary>
        protected override void OnSizeChanged(EventArgs e)
        {
            base.OnSizeChanged(e);
            _sliderlocation = GetPosition();
            ResizeColorBoard();
            Invalidate();
        }

        /// <summary>
        /// 创建一个类似Adobe拾色器风格的光标指针
        /// </summary>
        private System.Windows.Forms.Cursor CreateCursor()
        {
            Bitmap cur = new Bitmap(18, 18);
            Graphics g = Graphics.FromImage(cur);
            g.SmoothingMode = SmoothingMode.AntiAlias;
            Pen pen = new Pen(Color.White, 2.0f);
            g.DrawEllipse(pen, 2, 2, 14, 14);
            g.DrawEllipse(Pens.Black, 1, 1, 16, 16);
            g.Dispose();
            pen.Dispose();
            return new System.Windows.Forms.Cursor(cur.GetHicon());
        }

        /// <summary>
        /// 根据此ColorDrawer的大小，以及绘制类型调整调色板的大小。
        /// </summary>
        private void ResizeColorBoard()
        {
            if (ClientRectangle.Width <= 0 || ClientRectangle.Height <= 0) return;
            if (_colortablepath != null) _colortablepath.Dispose();
            if (_colortablebrush != null) _colortablebrush.Dispose();
            if (_backTexture != null) _backTexture.Dispose();
            Point[] p = new Point[4];
            p[0] = new Point(0, 0);
            p[1] = new Point(ClientRectangle.Width, 0);
            p[2] = new Point(ClientRectangle.Width, ClientRectangle.Height);
            p[3] = new Point(0, ClientRectangle.Height);
            _colortablepath = new GraphicsPath();
            _colortablepath.AddPolygon(p);
            _colortablebrush = new PathGradientBrush(_colortablepath);
            if (base.BackgroundImage != null) _backTexture = new TextureBrush(base.BackgroundImage, new Rectangle(0, 0, BackgroundImage.Width, BackgroundImage.Height));
        }

        /// <summary>
        /// 绘制颜色滑标
        /// </summary>
        protected virtual void DrawSlider(Graphics g, Point p)
        {
            Pen pen;
            Rectangle rect = new Rectangle(p.X - 5, p.Y - 5, 10, 10);
            //使用p点位置颜色的高对比度颜色来绘制滑标
            if (_hsb.B < 0.8d)
                pen = new Pen(Color.White);
            else if (_hsb.H < 0.1d || _hsb.H > 0.5d)
                if (_hsb.S > 0.3d)
                    pen = new Pen(Color.White);
                else
                    pen = new Pen(Color.Black);
            else
                pen = new Pen(Color.Black);
            SmoothingMode sm = g.SmoothingMode;
            CompositingQuality cq = g.CompositingQuality;
            g.CompositingQuality = CompositingQuality.GammaCorrected;
            g.SmoothingMode = SmoothingMode.AntiAlias;
            g.DrawEllipse(pen, rect);
            g.SmoothingMode = sm;
            g.CompositingQuality = cq;
            pen.Dispose();
        }

        /// <summary>
        /// 对应RGB颜色空间Red颜色选取模式
        /// </summary>
        protected virtual void DrawStyleRed(Color value, Graphics g, GraphicsPath path)
        {
            _colortablebrush.SurroundColors = new Color[4] {
                Color.FromArgb(value.A, value.R, 255, 0),
                Color.FromArgb(value.A, value.R, 255, 255),
                Color.FromArgb(value.A, value.R, 0, 255),
                Color.FromArgb(value.A, value.R, 0, 0) };
            _colortablebrush.CenterPoint = _colortablepath.PathPoints[0];
            _colortablebrush.CenterColor = _colortablebrush.SurroundColors[0];
            g.FillPath(_colortablebrush, path);
        }

        protected virtual void DrawStyleRed(Color value, Graphics g, Rectangle rect)
        {
            using (GraphicsPath path = new GraphicsPath())
            {
                path.AddRectangle(rect);
                DrawStyleRed(value, g, path);
            }
        }

        /// <summary>
        /// 对应RGB颜色空间Green颜色选取模式
        /// </summary>
        protected virtual void DrawStyleGreen(Color value, Graphics g, GraphicsPath path)
        {
            _colortablebrush.SurroundColors = new Color[] { Color.FromArgb(value.A, 255, value.G, 0), Color.FromArgb(value.A, 255, value.G, 255), Color.FromArgb(value.A, 0, value.G, 255), Color.FromArgb(value.A, 0, value.G, 0) };
            _colortablebrush.CenterPoint = _colortablepath.PathPoints[0];
            _colortablebrush.CenterColor = _colortablebrush.SurroundColors[0];
            g.FillPath(_colortablebrush, path);
        }

        protected virtual void DrawStyleGreen(Color value, Graphics g, Rectangle rect)
        {
            using (GraphicsPath path = new GraphicsPath())
            {
                path.AddRectangle(rect);
                DrawStyleGreen(value, g, path);
            }
        }

        /// <summary>
        /// 对应RGB颜色空间Blue颜色选取模式
        /// </summary>
        protected virtual void DrawStyleBlue(Color value, Graphics g, GraphicsPath path)
        {
            if (_colortablepath == null) ResizeColorBoard();
            _colortablebrush.SurroundColors = new Color[4] { Color.FromArgb(value.A, 0, 255, value.B), Color.FromArgb(value.A, 255, 255, value.B), Color.FromArgb(value.A, 255, 0, value.B), Color.FromArgb(value.A, 0, 0, value.B) };
            _colortablebrush.CenterPoint = _colortablepath.PathPoints[0];
            _colortablebrush.CenterColor = _colortablebrush.SurroundColors[0];
            g.FillPath(_colortablebrush, path);
        }

        /// <summary>
        ///
        /// </summary>
        protected virtual void DrawStyleBlue(Color value, Graphics g, Rectangle rect)
        {
            using (GraphicsPath path = new GraphicsPath())
            {
                path.AddRectangle(rect);
                DrawStyleBlue(value, g, path);
            }
        }

        /// <summary>
        ///
        /// </summary>
        protected virtual void DrawStyleBlue(Color value, Graphics g)
        {
            DrawStyleBlue(value, g, _colortablepath);
        }

        /// <summary>
        /// 对应HSB颜色空间的Hue颜色选取模式
        /// </summary>
        /// <param name="value">HSB颜色对象</param>
        /// <param name="g">绘图表面</param>
        /// <param name="rect">ColorDrawer客户区的一个抽样矩形</param>
        protected virtual void DrawStyleHue(ColorSpace.HSB value, Graphics g, Rectangle rect)
        {
            LinearGradientBrush _lbr = new LinearGradientBrush(new Rectangle(rect.X - 1, rect.Y, rect.Width + 2, 1), Color.White, Color.Black, 0, false);
            Rectangle _rect;
            ColorSpace.HSB _hsb1 = value;
            ColorSpace.HSB _hsb2 = value;
            _hsb1.S = (double)rect.X / (double)ClientRectangle.Width;
            _hsb2.S = (double)rect.Right / (double)ClientRectangle.Width;
            for (int i = rect.Y; i < rect.Bottom; i++)				//	逐行扫描rect
            {
                _hsb1.B = 1.0d - (double)i / ClientRectangle.Height;
                _hsb2.B = _hsb1.B;
                _rect = new Rectangle(rect.X, i, rect.Width, 1);
                _lbr.LinearColors = new Color[] { _hsb1.ToRGB(), _hsb2.ToRGB() };
                g.FillRectangle(_lbr, _rect);
            }
            _lbr.Dispose();
        }

        /// <summary>
        /// 对应HSB颜色空间的Saturation颜色选取模式
        /// </summary>
        protected virtual void DrawStyleSaturation(ColorSpace.HSB value, Graphics g, Rectangle rect)
        {
            LinearGradientBrush _lbr = new LinearGradientBrush(new Rectangle(rect.X, rect.Y - 1, 1, rect.Height + 2), Color.White, Color.Black, 90, false);
            Rectangle _rect;
            ColorSpace.HSB _hsb1 = value;
            ColorSpace.HSB _hsb2 = value;
            _hsb1.B = 1.0d - (double)rect.Y / (double)ClientRectangle.Height;
            _hsb2.B = 1.0d - (double)rect.Bottom / (double)ClientRectangle.Height;
            for (int i = rect.X; i < rect.Right; i++)
            {
                _hsb1.H = (double)i / ClientRectangle.Width;
                _hsb2.H = _hsb1.H;
                _rect = new Rectangle(i, rect.Y, 1, rect.Height);
                _lbr.LinearColors = new Color[] { _hsb1.ToRGB(), _hsb2.ToRGB() };
                g.FillRectangle(_lbr, _rect);
            }
            _lbr.Dispose();
        }

        /// <summary>
        /// 对应HSB颜色空间的Brightness颜色选取模式
        /// </summary>
        protected virtual void DrawStyleBrightness(ColorSpace.HSB value, Graphics g, Rectangle rect)
        {
            LinearGradientBrush _lbr = new LinearGradientBrush(new Rectangle(rect.X, rect.Y - 1, 1, rect.Height + 2), Color.White, Color.Black, 90, false);
            Rectangle _rect;
            ColorSpace.HSB _hsb1 = value;
            ColorSpace.HSB _hsb2 = value;
            _hsb1.S = 1.0d - (double)rect.Y / (double)ClientRectangle.Height; ;
            _hsb2.S = 1.0d - (double)rect.Bottom / (double)ClientRectangle.Height; ;

            for (int i = rect.X; i < rect.Right; i++)
            {
                _hsb1.H = (double)i / ClientRectangle.Width;
                _hsb2.H = _hsb1.H;
                _rect = new Rectangle(i, rect.Y, 1, rect.Height);
                _lbr.LinearColors = new Color[] { _hsb1.ToRGB(), _hsb2.ToRGB() };
                g.FillRectangle(_lbr, _rect);
            }
            _lbr.Dispose();
        }

        /// <summary>
        /// 对应Lab颜色空间L颜色选取模式
        /// </summary>
        protected virtual void DrawStyleLabL(ColorSpace.LAB value, Graphics g, Rectangle rect)
        {
            Rectangle _rect;
            int p;
            ColorSpace.LAB lab = value;
            int y = (int)Math.Floor((double)ClientRectangle.Height / 255.0) + 1;
            if (y <= 0) y = 1;
            int x = (int)Math.Floor((double)ClientRectangle.Width / 255.0) + 1;
            if (x <= 0) x = 1;
            LinearGradientBrush _lbr = new LinearGradientBrush(new Rectangle(rect.X - 1, rect.Y, rect.Width + 2, y), Color.White, Color.Black, 0, false);
            ColorBlend bd = new ColorBlend((int)Math.Floor((double)rect.Width / (double)x) + 1);
            for (int i = rect.Y; i < rect.Bottom; i += y)
            {
                lab.B = 127 - (double)i / ClientRectangle.Height * 255;
                _rect = new Rectangle(rect.X, i, rect.Width, y);
                p = 0;
                for (int j = rect.X; j <= rect.Right; j += x)
                {
                    lab.A = (double)j / ClientRectangle.Width * 255 - 128;
                    bd.Colors[p] = lab.ToRGB();
                    bd.Positions[p] = (float)p / (float)bd.Positions.Length;
                    p++;
                }
                bd.Positions[bd.Positions.Length - 1] = 1;
                if (bd.Positions.Length > 2)
                    _lbr.InterpolationColors = bd;
                else
                    _lbr.LinearColors = new Color[] { bd.Colors[0], bd.Colors[0] };
                g.FillRectangle(_lbr, _rect);
            }
            _lbr.Dispose();
        }

        /// <summary>
        /// 对应Lab颜色空间a颜色选取模式
        /// </summary>
        protected virtual void DrawStyleLaba(ColorSpace.LAB value, Graphics g, Rectangle rect)
        {
            Rectangle _rect;
            int p;
            ColorSpace.LAB lab = value;
            int y = (int)Math.Floor((double)ClientRectangle.Height / 255.0) + 1;
            if (y <= 0) y = 1;
            int x = (int)Math.Floor((double)ClientRectangle.Width / 255.0) + 1;
            if (x <= 0) x = 1;
            LinearGradientBrush _lbr = new LinearGradientBrush(new Rectangle(rect.X - 1, rect.Y, rect.Width + 2, y), Color.White, Color.Black, 0, false);
            ColorBlend bd = new ColorBlend((int)Math.Floor((double)rect.Width / (double)x) + 1);
            for (int i = rect.Y; i < rect.Bottom; i += y)
            {
                lab.L = (1 - (double)i / ClientRectangle.Height) * 100;
                _rect = new Rectangle(rect.X, i, rect.Width, y);
                p = 0;
                for (int j = rect.X; j <= rect.Right; j += x)
                {
                    lab.B = (double)j / ClientRectangle.Width * 255 - 128;
                    bd.Colors[p] = lab.ToRGB();
                    bd.Positions[p] = (float)p / (float)bd.Positions.Length;
                    p++;
                }
                bd.Positions[bd.Positions.Length - 1] = 1;
                if (bd.Positions.Length > 2)
                    _lbr.InterpolationColors = bd;
                else
                    _lbr.LinearColors = new Color[] { bd.Colors[0], bd.Colors[0] };
                g.FillRectangle(_lbr, _rect);
            }
            _lbr.Dispose();
        }

        /// <summary>
        /// 对应Lab颜色空间b颜色选取模式
        /// </summary>
        protected virtual void DrawStyleLabb(ColorSpace.LAB value, Graphics g, Rectangle rect)
        {
            Rectangle _rect;
            int p;
            ColorSpace.LAB lab = value;
            int y = (int)Math.Floor((double)ClientRectangle.Height / 255.0) + 1;
            if (y <= 0) y = 1;
            int x = (int)Math.Floor((double)ClientRectangle.Width / 255.0) + 1;
            if (x <= 0) x = 1;
            LinearGradientBrush _lbr = new LinearGradientBrush(new Rectangle(rect.X - 1, rect.Y, rect.Width + 2, y), Color.White, Color.Black, 0, false);
            ColorBlend bd = new ColorBlend((int)Math.Floor((double)rect.Width / (double)x) + 1);
            for (int i = rect.Y; i < rect.Bottom; i += y)
            {
                lab.L = (1 - (double)i / ClientRectangle.Height) * 100;
                _rect = new Rectangle(rect.X, i, rect.Width, y);
                p = 0;
                for (int j = rect.X; j <= rect.Right; j += x)
                {
                    lab.A = (double)j / ClientRectangle.Width * 255 - 128;
                    bd.Colors[p] = lab.ToRGB();
                    bd.Positions[p] = (float)p / (float)bd.Positions.Length;
                    p++;
                }
                bd.Positions[bd.Positions.Length - 1] = 1;
                if (bd.Positions.Length > 2)
                    _lbr.InterpolationColors = bd;
                else
                    _lbr.LinearColors = new Color[] { bd.Colors[0], bd.Colors[0] };
                g.FillRectangle(_lbr, _rect);
            }
            _lbr.Dispose();
        }

        /// <summary>
        /// 根据当前的颜色选取模式计算滑标应该处于的位置
        /// </summary>
        public Point GetPosition()
        {
            double ratex = 0, ratey = 0;
            switch (_colorTableType)
            {
                case ColorTableType.Red:
                    ratey = (255.0 - _color.G) / 255.0;
                    ratex = _color.B / 255.0;
                    break;

                case ColorTableType.Green:
                    ratey = (255.0 - _color.R) / 255.0;
                    ratex = _color.B / 255.0;
                    break;

                case ColorTableType.Blue:
                    ratex = _color.R / 255.0;
                    ratey = (255.0 - _color.G) / 255.0;
                    break;

                case ColorTableType.Hue:
                    ratex = _hsb.S;
                    ratey = 1.0 - _hsb.B;
                    break;

                case ColorTableType.Saturation:
                    ratex = _hsb.H;
                    ratey = 1.0 - _hsb.B;
                    break;

                case ColorTableType.Brightness:
                    ratex = _hsb.H;
                    ratey = 1.0 - _hsb.S;
                    break;

                case ColorTableType.LabL:
                    ratex = (_lab.A + 128.0) / 255.0;
                    ratey = (127.0 - _lab.B) / 255.0;
                    break;

                case ColorTableType.Laba:
                    ratey = (100.0 - _lab.L) / 100.0;
                    ratex = (_lab.B + 128.0) / 255.0;
                    break;

                case ColorTableType.Labb:
                    ratey = (100.0 - _lab.L) / 100.0;
                    ratex = (_lab.A + 128.0) / 255.0;
                    break;

                default:
                    throw new NotImplementedException(_colorTableType.ToString());
            }
            return new Point(Convert.ToInt32(ratex * (double)ClientRectangle.Width), Convert.ToInt32(ratey * (double)ClientRectangle.Height));
        }

        /// <summary>
        /// 获取在指定位置p点的RGB颜色,并设置色谱各空间的色彩采样值
        /// </summary>
        public Color GetColor(Point p)
        {
            double ratex = 0, ratey = 0;
            ratex = (double)p.X / (double)ClientRectangle.Width;
            ratey = (double)p.Y / (double)ClientRectangle.Height;
            switch (_colorTableType)
            {
                case ColorTableType.Red:
                    _color = Color.FromArgb(_color.A, _color.R, Convert.ToInt32(255.0 - ratey * 255.0), Convert.ToInt32(ratex * 255.0));
                    _lab = ColorSpace.LAB.FromRGB(_color);
                    _hsb = ColorSpace.HSB.FromRGB(_color);
                    break;

                case ColorTableType.Green:
                    _color = Color.FromArgb(_color.A, Convert.ToInt32(255.0 - ratey * 255.0), _color.G, Convert.ToInt32(ratex * 255.0));
                    _lab = ColorSpace.LAB.FromRGB(_color);
                    _hsb = ColorSpace.HSB.FromRGB(_color);
                    break;

                case ColorTableType.Blue:
                    _color = Color.FromArgb(_color.A, Convert.ToInt32(ratex * 255.0), Convert.ToInt32(255.0 - ratey * 255.0), _color.B);
                    _lab = ColorSpace.LAB.FromRGB(_color);
                    _hsb = ColorSpace.HSB.FromRGB(_color);
                    break;

                case ColorTableType.Hue:
                    _hsb.S = ratex;
                    _hsb.B = 1.0 - ratey;
                    _color = (Color)_hsb;
                    _lab = ColorSpace.LAB.FromRGB(_color);
                    break;

                case ColorTableType.Saturation:
                    _hsb.H = ratex;
                    _hsb.B = 1.0 - ratey;
                    _color = (Color)_hsb;
                    _lab = ColorSpace.LAB.FromRGB(_color);
                    break;

                case ColorTableType.Brightness:
                    _hsb.H = ratex;
                    _hsb.S = 1.0 - ratey;
                    _color = (Color)_hsb;
                    _lab = ColorSpace.LAB.FromRGB(_color);
                    break;

                case ColorTableType.LabL:
                    _lab.A = ratex * 255 - 128;
                    _lab.B = 127 - ratey * 255;
                    _color = _lab.ToRGB();
                    _hsb = ColorSpace.HSB.FromRGB(_color);
                    break;

                case ColorTableType.Laba:
                    _lab.L = 100 - ratey * 100;
                    _lab.B = ratex * 255 - 128;
                    _color = _lab.ToRGB();
                    _hsb = ColorSpace.HSB.FromRGB(_color);
                    break;

                case ColorTableType.Labb:
                    _lab.L = 100 - ratey * 100;
                    _lab.A = ratex * 255 - 128;
                    _color = _lab.ToRGB();
                    _hsb = ColorSpace.HSB.FromRGB(_color);
                    break;

                default:
                    throw new NotImplementedException(_colorTableType.ToString());
            }
            return _color;
        }

        protected void SyncSlider(Point p)
        {
            Point pos = Point.Empty;
            pos.X = p.X < 0 ? 0 : p.X > ClientRectangle.Right ? ClientRectangle.Right : p.X;
            pos.Y = p.Y < 0 ? 0 : p.Y > ClientRectangle.Bottom ? ClientRectangle.Bottom : p.Y;
            if (_sliderlocation != pos)
            {
                GetColor(pos);
                Graphics g = CreateGraphics();
                Invalidate(new Rectangle(new Point(_sliderlocation.X - 6, _sliderlocation.Y - 6), new Size(12, 12)));
                //OnPaint(new PaintEventArgs(g, new Rectangle(new Point(_sliderlocation.X - 6, _sliderlocation.Y - 6), new Size(12, 12))));
                _sliderlocation = pos;
                DrawSlider(g, pos);
                g.Dispose();
                Update();
                Application.DoEvents();
                OnSliderScroll(EventArgs.Empty);
            }
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            switch (_colorTableType)
            {
                case ColorTableType.Red:
                    DrawStyleRed(_color, e.Graphics, e.ClipRectangle);
                    break;

                case ColorTableType.Green:
                    DrawStyleGreen(_color, e.Graphics, e.ClipRectangle);
                    break;

                case ColorTableType.Blue:
                    DrawStyleBlue(_color, e.Graphics, e.ClipRectangle);
                    break;

                case ColorTableType.Hue:
                    DrawStyleHue(_hsb, e.Graphics, e.ClipRectangle);
                    break;

                case ColorTableType.Saturation:
                    DrawStyleSaturation(_hsb, e.Graphics, e.ClipRectangle);
                    break;

                case ColorTableType.Brightness:
                    DrawStyleBrightness(_hsb, e.Graphics, e.ClipRectangle);
                    break;

                case ColorTableType.LabL:
                    DrawStyleLabL(_lab, e.Graphics, e.ClipRectangle);
                    break;

                case ColorTableType.Laba:
                    DrawStyleLaba(_lab, e.Graphics, e.ClipRectangle);
                    break;

                case ColorTableType.Labb:
                    DrawStyleLabb(_lab, e.Graphics, e.ClipRectangle);
                    break;

                default:
                    throw new NotImplementedException(_colorTableType.ToString());
            }
            DrawSlider(e.Graphics, _sliderlocation);
        }

        /// <summary>
        /// 移动鼠标
        /// </summary>
        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);
            if (e.Button == MouseButtons.Left)
            {
                SyncSlider(e.Location);
            }
        }

        /// <summary>
        /// 鼠标被按下
        /// </summary>
        protected override void OnMouseDown(MouseEventArgs e)
        {
            base.OnMouseMove(e);
            if (e.Button == MouseButtons.Left)
            {
                SyncSlider(e.Location);
            }
        }

        /// <summary>
        /// 引发ColorChangedEvent事件
        /// </summary>
        protected virtual void OnColorChanged(EventArgs e)
        {
            _sliderlocation = GetPosition();
            Invalidate();
            if (ColorChanged != null) ColorChanged(this, e);
        }

        /// <summary>
        /// 引发SliderScroll事件
        /// </summary>
        protected virtual void OnSliderScroll(EventArgs e)
        {
            if (SliderScroll != null) SliderScroll(this, e);
        }
    }
}