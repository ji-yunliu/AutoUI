using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.ComponentModel;
using System.Runtime.InteropServices;

namespace ColorPane
{
    [ToolboxItem(false)]
    public class ColorSlider : Control
    {
        public event SliderEventHandler SliderScroll;

        public event SliderEventHandler SliderActivated;

        public event EventHandler ColorChanged;

        /// <summary>
        /// 将ColorSlider呈现的方向
        /// </summary>
        private Orientation _orientation = Orientation.Vertical;

        /// <summary>
        /// 颜色选择模式
        /// </summary>
        private ColorTableType _colorTableType = ColorTableType.Blue;

        private bool _alphaSelect;//Alpha通道选择
        private Bitmap _backTexture;//背景

        private List<Slider> _sliders;
        private Slider _activatedSlider;//表示活动滑标

        private SliderSide _sliderVisible = SliderSide.Both;//滑标显示的位置
        private bool _spectrumMaker = false;//指示是否渐变配制功能；

        private Color _borderColor = Color.Blue;//指示色谱区域边框的颜色
        private float _borderWidth = 1.2f;//色谱边框的宽度
        private Rectangle _viewBounds;//表示色谱使用的区域边框
        private Rectangle _oldViewBounds;//表示色谱使用区域边框旧值（在ColorSlider的Size改变时使用）
        private LinearGradientBrush _brush;//表示绘制色谱的画刷

        private Color _pickedColor;//选择的颜色；

        /// <summary>
        /// RGB颜色
        /// </summary>
        private Color _rgb;

        /// <summary>
        /// HSB颜色
        /// </summary>
        private ColorSpace.HSB _hsb;

        /// <summary>
        /// Lab颜色
        /// </summary>
        private ColorSpace.LAB _lab;

        public ColorSlider()
            : base()
        {
            SetStyle(ControlStyles.Selectable | ControlStyles.OptimizedDoubleBuffer | ControlStyles.SupportsTransparentBackColor | ControlStyles.AllPaintingInWmPaint | ControlStyles.ResizeRedraw, true);
            _hsb = ColorSpace.HSB.FromHSB(1.0d, 1.0d, 1.0d);
            _rgb = _hsb.ToRGB();
            _lab = ColorSpace.RGB2LAB(_rgb);
            CreateViewBounds();
            _sliders = new List<Slider>();
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            if (disposing)
            {
                if (_brush != null) _brush.Dispose();
            }
            GC.Collect();
        }

        /// <summary>
        /// 创建或更新色谱区域边框
        /// </summary>
        private void CreateViewBounds()
        {
            if (ClientRectangle.Width < 5 || ClientRectangle.Height < 5) return;
            if (_orientation == Orientation.Vertical)
            {
                switch (_sliderVisible)
                {
                    case SliderSide.Both:
                        _viewBounds = new Rectangle(ClientRectangle.X + 8, ClientRectangle.Y, ClientRectangle.Width - 17, ClientRectangle.Height - 1);
                        return;

                    case SliderSide.LeftOrTop:
                        _viewBounds = new Rectangle(ClientRectangle.X + 8, ClientRectangle.Y, ClientRectangle.Width - 9, ClientRectangle.Height - 1);
                        return;

                    case SliderSide.RightOrBottom:
                        _viewBounds = new Rectangle(ClientRectangle.X, ClientRectangle.Y, ClientRectangle.Width - 9, ClientRectangle.Height - 1);
                        return;
                }
            }
            else
            {
                switch (_sliderVisible)
                {
                    case SliderSide.Both:
                        _viewBounds = new Rectangle(ClientRectangle.X, ClientRectangle.Y + 8, ClientRectangle.Width - 1, ClientRectangle.Height - 17);
                        return;

                    case SliderSide.LeftOrTop:
                        _viewBounds = new Rectangle(ClientRectangle.X, ClientRectangle.Y + 8, ClientRectangle.Width - 1, ClientRectangle.Height - 9);
                        return;

                    case SliderSide.RightOrBottom:
                        _viewBounds = new Rectangle(ClientRectangle.X, ClientRectangle.Y, ClientRectangle.Width - 1, ClientRectangle.Height - 9);
                        return;
                }
            }
            throw new NotImplementedException();
        }

        /// <summary>
        /// 创建并更新绘制色谱可能使用的画刷
        /// </summary>
        private void CreateBrush()
        {
            if (_viewBounds.Width <= 0 || _viewBounds.Height <= 0) return;
            float angle = _orientation == Orientation.Vertical ? 90f : 0f;
            if (_brush != null) _brush.Dispose();
            _brush = new LinearGradientBrush(_viewBounds, Color.Transparent, _rgb, angle, false);
        }

        protected virtual void DrawBackTexture(Graphics g)
        {
            if (_backTexture == null) return;
            TextureBrush brush = new TextureBrush(_backTexture, WrapMode.Tile);
            g.FillRectangle(brush, _viewBounds);
            brush.Dispose();
        }

        protected virtual void DrawStyleAlphaSelect(Graphics g)
        {
            _brush.LinearColors = new Color[] { Color.Transparent, Color.FromArgb(255, _rgb) };
            g.FillRectangle(_brush, _viewBounds);
        }

        protected virtual void DrawStyleSpectrumMaker(Graphics g)
        {
            if (_sliders.Count >= 2) _brush.InterpolationColors = GetColorBlend();
            g.FillRectangle(_brush, _viewBounds);
        }

        protected virtual void DrawStyleRed(Graphics g)
        {
            _brush.LinearColors = new Color[] { Color.FromArgb(_rgb.A, 255, _rgb.G, _rgb.B), Color.FromArgb(_rgb.A, 0, _rgb.G, _rgb.B) };
            g.FillRectangle(_brush, _viewBounds);
        }

        protected virtual void DrawStyleGreen(Graphics g)
        {
            _brush.LinearColors = new Color[] { Color.FromArgb(_rgb.A, _rgb.R, 255, _rgb.B), Color.FromArgb(_rgb.A, _rgb.R, 0, _rgb.B) };
            g.FillRectangle(_brush, _viewBounds);
        }

        protected virtual void DrawStyleBlue(Graphics g)
        {
            _brush.LinearColors = new Color[] { Color.FromArgb(_rgb.A, _rgb.R, _rgb.G, 255), Color.FromArgb(_rgb.A, _rgb.R, _rgb.G, 0) };
            g.FillRectangle(_brush, _viewBounds);
        }

        protected virtual void DrawStyleHue(Graphics g)
        {
            Pen pen = new Pen(Color.Transparent);
            if (_orientation == Orientation.Vertical)
            {
                for (int i = 0; i < _viewBounds.Height; i++)
                {
                    pen.Color = ColorSpace.HSB.FromHSB(_hsb.A, 1.0d - (double)i / (double)_viewBounds.Height, 1.0d, 1.0d).ToRGB();
                    g.DrawLine(pen, _viewBounds.X, _viewBounds.Y + i, _viewBounds.Right, _viewBounds.Y + i);
                }
            }
            else
            {
                for (int i = 0; i < _viewBounds.Width; i++)
                {
                    pen.Color = ColorSpace.HSB.FromHSB(_hsb.A, 1.0d - (double)i / (double)_viewBounds.Width, 1.0d, 1.0d).ToRGB();
                    g.DrawLine(pen, _viewBounds.X + i, _viewBounds.Y, _viewBounds.X + i, _viewBounds.Bottom);
                }
            }
        }

        protected virtual void DrawStyleSaturation(Graphics g)
        {
            Pen pen = new Pen(Color.Transparent);
            if (_orientation == Orientation.Vertical)
            {
                for (int i = 0; i < _viewBounds.Height; i++)
                {
                    pen.Color = ColorSpace.HSB.FromHSB(_hsb.A, _hsb.H, 1.0d - (double)i / (double)_viewBounds.Height, _hsb.B).ToRGB();
                    g.DrawLine(pen, _viewBounds.X, _viewBounds.Y + i, _viewBounds.Right, _viewBounds.Y + i);
                }
            }
            else
            {
                for (int i = 0; i < _viewBounds.Width; i++)
                {
                    pen.Color = ColorSpace.HSB.FromHSB(_hsb.A, _hsb.H, 1.0d - (double)i / (double)_viewBounds.Width, _hsb.B).ToRGB();
                    g.DrawLine(pen, _viewBounds.X + i, _viewBounds.Y, _viewBounds.X + i, _viewBounds.Bottom);
                }
            }
        }

        protected virtual void DrawStyleBrightness(Graphics g)
        {
            Pen pen = new Pen(Color.Transparent);
            if (_orientation == Orientation.Vertical)
            {
                for (int i = 0; i < _viewBounds.Height; i++)
                {
                    pen.Color = ColorSpace.HSB.FromHSB(_hsb.A, _hsb.H, _hsb.S, 1.0d - (double)i / (double)_viewBounds.Height).ToRGB();
                    g.DrawLine(pen, _viewBounds.X, _viewBounds.Y + i, _viewBounds.Right, _viewBounds.Y + i);
                }
            }
            else
            {
                for (int i = 0; i < _viewBounds.Width; i++)
                {
                    pen.Color = ColorSpace.HSB.FromHSB(_hsb.A, _hsb.H, _hsb.S, 1.0d - (double)i / (double)_viewBounds.Width).ToRGB();
                    g.DrawLine(pen, _viewBounds.X + i, _viewBounds.Y, _viewBounds.X + i, _viewBounds.Bottom);
                }
            }
        }

        protected virtual void DrawStyleLabL(Graphics g)
        {
            Pen pen = new Pen(Color.Transparent);
            ColorSpace.LAB tmplab = _lab;
            if (_orientation == Orientation.Vertical)
            {
                for (int i = 0; i < _viewBounds.Height; i++)
                {
                    tmplab.L = (1.0d - (double)i / (double)_viewBounds.Height) * 100;
                    pen.Color = tmplab.ToRGB();
                    g.DrawLine(pen, _viewBounds.X, _viewBounds.Y + i, _viewBounds.Right, _viewBounds.Y + i);
                }
            }
            else
            {
                for (int i = 0; i < _viewBounds.Width; i++)
                {
                    tmplab.L = (1.0d - (double)i / (double)_viewBounds.Width) * 100;
                    pen.Color = tmplab.ToRGB();
                    g.DrawLine(pen, _viewBounds.X + i, _viewBounds.Y, _viewBounds.X + i, _viewBounds.Bottom);
                }
            }
        }

        protected virtual void DrawStyleLaba(Graphics g)
        {
            Pen pen = new Pen(Color.Transparent);
            ColorSpace.LAB tmplab = _lab;
            if (_orientation == Orientation.Vertical)
            {
                for (int i = 0; i < _viewBounds.Height; i++)
                {
                    tmplab.A = 127.0 - (double)i / (double)_viewBounds.Height * 255.0;
                    pen.Color = tmplab.ToRGB();
                    g.DrawLine(pen, _viewBounds.X, _viewBounds.Y + i, _viewBounds.Right, _viewBounds.Y + i);
                }
            }
            else
            {
                for (int i = 0; i < _viewBounds.Width; i++)
                {
                    tmplab.A = 127.0 - (double)i / (double)_viewBounds.Width * 255.0;
                    pen.Color = tmplab.ToRGB();
                    g.DrawLine(pen, _viewBounds.X + i, _viewBounds.Y, _viewBounds.X + i, _viewBounds.Bottom);
                }
            }
        }

        protected virtual void DrawStyleLabb(Graphics g)
        {
            Pen pen = new Pen(Color.Transparent);
            ColorSpace.LAB tmplab = _lab;
            if (_orientation == Orientation.Vertical)
            {
                for (int i = 0; i < _viewBounds.Height; i++)
                {
                    tmplab.B = 127.0 - (double)i / (double)_viewBounds.Height * 255.0;
                    pen.Color = tmplab.ToRGB();
                    g.DrawLine(pen, _viewBounds.X, _viewBounds.Y + i, _viewBounds.Right, _viewBounds.Y + i);
                }
            }
            else
            {
                for (int i = 0; i < _viewBounds.Width; i++)
                {
                    tmplab.B = 127.0 - (double)i / (double)_viewBounds.Width * 255.0;
                    pen.Color = tmplab.ToRGB();
                    g.DrawLine(pen, _viewBounds.X + i, _viewBounds.Y, _viewBounds.X + i, _viewBounds.Bottom);
                }
            }
        }

        protected virtual void DrawViewBorder(Graphics g)
        {
            Pen pencil = new Pen(_borderColor, _borderWidth);
            g.DrawRectangle(pencil, _viewBounds);
            pencil.Dispose();
        }

        private void ClearSliders(Graphics g)
        {
            Region region = new Region(ClientRectangle);
            region.Exclude(_viewBounds);
            SolidBrush brush = new SolidBrush(BackColor);
            g.FillRegion(brush, region);
            region.Dispose();
            brush.Dispose();
        }

        protected virtual void DrawSliders(Graphics g)
        {
            if (_sliders.Count > 0)
            {
                ClearSliders(g);
                Slider _slider;
                for (int i = 0; i < _sliders.Count; i++)
                {
                    _slider = _sliders[i];
                    DrawSlider(g, _slider);
                }
            }
        }

        protected virtual void DrawSlider(Graphics g, Slider slider)
        {
            PointF[] p; int pos = _orientation == Orientation.Vertical ? slider.Location.Y : slider.Location.X;
            if (slider.TriangleStyle) p = new PointF[3];
            else p = new PointF[7];

            SolidBrush brush = new SolidBrush(slider.Color);
            Pen pen = new Pen(slider.Border);
            if (slider == _activatedSlider && _spectrumMaker) pen.Color = SystemColors.HotTrack;
            if (_orientation == Orientation.Vertical)
            {
                #region 垂直方向的滑标

                if (_sliderVisible == SliderSide.Both || _sliderVisible == SliderSide.LeftOrTop)
                {
                    if (slider.TriangleStyle)
                    {
                        p[0] = new PointF(2f, (float)pos - 5f);
                        p[1] = new PointF(7f, (float)pos);
                        p[2] = new PointF(2f, (float)pos + 5f);
                    }
                    else
                    {
                        p[0] = new PointF(1f, (float)pos - 4);
                        p[1] = new PointF(3f, (float)pos - 4);
                        p[2] = new PointF(7f, (float)pos);
                        p[3] = new PointF(3f, (float)pos + 4);
                        p[4] = new PointF(1f, (float)pos + 4);
                        p[5] = new PointF(0f, (float)pos + 3);
                        p[6] = new PointF(0f, (float)pos - 3);
                    }
                    g.FillPolygon(brush, p);
                    g.DrawPolygon(pen, p);
                }
                if (_sliderVisible == SliderSide.Both || _sliderVisible == SliderSide.RightOrBottom)
                {
                    if (slider.TriangleStyle)
                    {
                        p[0] = new PointF((float)ClientRectangle.Width - 3f, (float)pos - 5f);
                        p[1] = new PointF((float)ClientRectangle.Width - 8f, (float)pos);
                        p[2] = new PointF((float)ClientRectangle.Width - 3f, (float)pos + 5f);
                    }
                    else
                    {
                        p[0] = new PointF((float)ClientRectangle.Width - 2f, (float)pos - 4f);
                        p[1] = new PointF((float)ClientRectangle.Width - 4f, (float)pos - 4f);
                        p[2] = new PointF((float)ClientRectangle.Width - 8f, (float)pos);
                        p[3] = new PointF((float)ClientRectangle.Width - 4f, (float)pos + 4f);
                        p[4] = new PointF((float)ClientRectangle.Width - 2f, (float)pos + 4f);
                        p[5] = new PointF((float)ClientRectangle.Width - 1f, (float)pos + 3f);
                        p[6] = new PointF((float)ClientRectangle.Width - 1f, (float)pos - 3f);
                    }
                    g.FillPolygon(brush, p);
                    g.DrawPolygon(pen, p);
                }

                #endregion 垂直方向的滑标
            }
            else
            {
                #region 水平方向的滑标

                if (_sliderVisible == SliderSide.Both || _sliderVisible == SliderSide.LeftOrTop)
                {
                    if (slider.TriangleStyle)
                    {
                        p[0] = new PointF((float)pos - 5f, 2f);
                        p[1] = new PointF((float)pos, 7f);
                        p[2] = new PointF((float)pos + 5f, 2f);
                    }
                    else
                    {
                        p[0] = new PointF((float)pos - 4, 1f);
                        p[1] = new PointF((float)pos - 4, 3f);
                        p[2] = new PointF((float)pos, 7f);
                        p[3] = new PointF((float)pos + 4, 3f);
                        p[4] = new PointF((float)pos + 4, 1f);
                        p[5] = new PointF((float)pos + 3, 0f);
                        p[6] = new PointF((float)pos - 3, 0f);
                    }
                    g.FillPolygon(brush, p);
                    g.DrawPolygon(pen, p);
                }
                if (_sliderVisible == SliderSide.Both || _sliderVisible == SliderSide.RightOrBottom)
                {
                    if (slider.TriangleStyle)
                    {
                        p[0] = new PointF((float)pos - 5f, (float)ClientRectangle.Height - 3f);
                        p[1] = new PointF((float)pos, (float)ClientRectangle.Height - 8f);
                        p[2] = new PointF((float)pos + 5f, (float)ClientRectangle.Height - 3f);
                    }
                    else
                    {
                        p[0] = new PointF((float)pos - 4f, (float)ClientRectangle.Height - 2f);
                        p[1] = new PointF((float)pos - 4f, (float)ClientRectangle.Height - 4f);
                        p[2] = new PointF((float)pos, (float)ClientRectangle.Height - 8f);
                        p[3] = new PointF((float)pos + 4f, (float)ClientRectangle.Height - 4f);
                        p[4] = new PointF((float)pos + 4f, (float)ClientRectangle.Height - 2f);
                        p[5] = new PointF((float)pos + 3f, (float)ClientRectangle.Height - 1f);
                        p[6] = new PointF((float)pos - 3f, (float)ClientRectangle.Height - 1f);
                    }
                    g.FillPolygon(brush, p);
                    g.DrawPolygon(pen, p);
                }

                #endregion 水平方向的滑标
            }
            brush.Dispose();
            pen.Dispose();
        }

        private void SyncSliders(Point pos)
        {
            Slider s;
            if (_spectrumMaker)
            {
                if (_sliders.Count < 2)
                {
                    s = new Slider(this);
                    s.Location = pos;
                    _sliders.Add(s);
                    _activatedSlider = s;
                }
                else if (_activatedSlider == null)
                {
                    s = new Slider(this);
                    s.Location = pos;
                    _sliders.Add(s);
                    _activatedSlider = s;
                }
                else
                {
                    _activatedSlider.Location = pos;
                    if (ClientRectangle.IntersectsWith(new Rectangle(new Point(pos.X - 64, pos.Y - 64), new Size(128, 128))))
                    {
                        if (!_sliders.Contains(_activatedSlider))
                        {
                            _sliders.Add(_activatedSlider);
                            Cursor = Cursors.Default;
                        }
                    }
                    else
                    {
                        if (_sliders.Contains(_activatedSlider))
                        {
                            _sliders.Remove(_activatedSlider);
                            //  Cursor = new Cursor(Properties.Resources.Cursor_Delete.GetHicon());
                        }
                    }
                }
                OnSliderScroll(new SliderEventArgs(_activatedSlider));
            }
            else
            {
                if (_sliders.Count == 0)
                {
                    s = new Slider(this);
                    s.Location = pos;
                    _sliders.Add(s);
                }
                else
                {
                    s = _sliders[0];
                    s.Location = pos;
                }
                OnSliderScroll(new SliderEventArgs(s));
            }
        }

        private void GetActiveSlider(Point pos)
        {
            if (_sliders.Count > 0)
            {
                Slider _slider = _activatedSlider = null;
                for (int i = 0; i < _sliders.Count; i++)
                {
                    _slider = _sliders[i];
                    if (_orientation == Orientation.Vertical)
                    {
                        if ((pos.Y < _slider.Location.Y + 5 && pos.Y > _slider.Location.Y - 5))
                        {
                            _activatedSlider = _slider;
                            return;
                        }
                    }
                    else
                    {
                        if ((pos.X < _slider.Location.X + 5 && pos.X > _slider.Location.X - 5))
                        {
                            _activatedSlider = _slider;
                            return;
                        }
                    }
                }//for
            }
        }

        private void MoveSliders()
        {
            Slider s;
            double percent = 0.0f;
            if (_sliders.Count > 0)
            {
                for (int i = 0; i < _sliders.Count; i++)
                {
                    s = _sliders[i];
                    if (_alphaSelect)
                    {
                        percent = (double)s.Color.A / 255.0f;
                    }
                    else if (_spectrumMaker)//内有多个滑标
                    {
                        if (_oldViewBounds == Rectangle.Empty) _oldViewBounds = _viewBounds;
                        if (_orientation == Orientation.Vertical)
                        {
                            if (_oldViewBounds.Height == 0) return;
                            percent = (double)s.Location.Y / (double)_oldViewBounds.Height;
                        }
                        else
                        {
                            if (_oldViewBounds.Width == 0) return;
                            percent = (double)s.Location.X / (double)_oldViewBounds.Width;
                        }
                    }
                    else//一定只有一个滑标
                    {
                        switch (_colorTableType)
                        {
                            case ColorTableType.Red:
                                percent = 1.0d - (double)_rgb.R / 255.0f;
                                break;

                            case ColorTableType.Green:
                                percent = 1.0d - (double)_rgb.G / 255.0f;
                                break;

                            case ColorTableType.Blue:
                                percent = 1.0d - (double)_rgb.B / 255.0f;
                                break;

                            case ColorTableType.Hue:
                                percent = 1.0d - _hsb.H;
                                break;

                            case ColorTableType.Saturation:
                                percent = 1.0d - _hsb.S;
                                break;

                            case ColorTableType.Brightness:
                                percent = 1.0d - _hsb.B;
                                break;

                            case ColorTableType.LabL:
                                percent = 1.0d - _lab.L / 100.0d;
                                break;

                            case ColorTableType.Laba:
                                percent = 1.0d - (_lab.A + 128) / 255.0d;
                                break;

                            case ColorTableType.Labb:
                                percent = 1.0d - (_lab.B + 128) / 255.0d;
                                break;

                            default:
                                throw new NotImplementedException();
                        }
                    }
                    if (_orientation == Orientation.Vertical)
                    {
                        s.Location = new Point(s.Location.X, _viewBounds.Top + (int)Math.Round((double)_viewBounds.Height * percent));
                    }
                    else
                    {
                        s.Location = new Point(_viewBounds.Left + (int)((double)_viewBounds.Width * percent), s.Location.Y);
                    }
                }
            }
            else
            {
                OnColorChanged(EventArgs.Empty);
            }
            _oldViewBounds = _viewBounds;
        }

        protected override void OnSizeChanged(EventArgs e)
        {
            base.OnSizeChanged(e);
            CreateViewBounds();
            CreateBrush();
            MoveSliders();
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            DrawBackTexture(e.Graphics);
            if (_alphaSelect)
            {
                DrawStyleAlphaSelect(e.Graphics);
            }
            else if (_spectrumMaker)
            {
                //DrawBackTexture(e.Graphics);
                DrawStyleSpectrumMaker(e.Graphics);
            }
            else
            {
                switch (_colorTableType)
                {
                    case ColorTableType.Red:
                        DrawStyleRed(e.Graphics);
                        break;

                    case ColorTableType.Green:
                        DrawStyleGreen(e.Graphics);
                        break;

                    case ColorTableType.Blue:
                        DrawStyleBlue(e.Graphics);
                        break;

                    case ColorTableType.Hue:
                        DrawStyleHue(e.Graphics);
                        break;

                    case ColorTableType.Saturation:
                        DrawStyleSaturation(e.Graphics);
                        break;

                    case ColorTableType.Brightness:
                        DrawStyleBrightness(e.Graphics);
                        break;

                    case ColorTableType.LabL:
                        DrawStyleLabL(e.Graphics);
                        break;

                    case ColorTableType.Laba:
                        DrawStyleLaba(e.Graphics);
                        break;

                    case ColorTableType.Labb:
                        DrawStyleLabb(e.Graphics);
                        break;

                    default:
                        throw new NotImplementedException();
                }
            }
            e.Graphics.SmoothingMode = SmoothingMode.HighQuality;
            DrawSliders(e.Graphics);
            DrawViewBorder(e.Graphics);
        }

        protected override void OnMouseDown(MouseEventArgs e)
        {
            base.OnMouseDown(e);
            if (e.Button == MouseButtons.Left && e.Clicks == 2)
            {
                GetActiveSlider(e.Location);
                if (_activatedSlider != null)
                {
                    OnSliderActivated(new SliderEventArgs(_activatedSlider));
                }
            }
            else if (e.Button == MouseButtons.Left)
            {
                GetActiveSlider(e.Location);
                SyncSliders(e.Location);
                Invalidate();
            }
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);
            if (e.Button == MouseButtons.Left)
            {
                SyncSliders(e.Location);
                Invalidate();
            }
        }

        protected override void OnMouseUp(MouseEventArgs e)
        {
            base.OnMouseUp(e);
            if (!_sliders.Contains(_activatedSlider) && _activatedSlider != null)
                _activatedSlider = null;
            Cursor = Cursors.Default;
        }

        protected virtual void OnSliderScroll(SliderEventArgs e)
        {
            if (e == null) throw new ArgumentNullException("e", "不能为null。");
            if (!_spectrumMaker)
            {
                GetColor(e.Slider.Location);
            }
            if (SliderScroll != null) SliderScroll(this, e);
        }

        protected virtual void OnSliderActivated(SliderEventArgs e)
        {
            if (SliderActivated != null) SliderActivated(this, e);
        }

        protected virtual void OnColorChanged(EventArgs e)
        {
            Slider s;
            if (!Capture)
            {
                if (_sliders.Count == 0)
                {
                    s = new Slider(this);
                    _sliders.Add(s);
                }
                else
                {
                    s = _sliders[0];
                }
                s.Location = GetPosition();
            }
            //MoveSliders();
            if (ColorChanged != null) ColorChanged(this, e);
        }

        public ColorBlend GetColorBlend()
        {
            List<Slider> _ls = new List<Slider>(_sliders);
            _ls.Sort(new SliderSorter(_orientation));
            ColorBlend ret = new ColorBlend();
            float[] pos = new float[_ls.Count + 2];
            Color[] color = new Color[_ls.Count + 2];
            for (int i = 0; i < _ls.Count; i++)
            {
                if (_orientation == Orientation.Vertical)
                    pos[i + 1] = (float)_ls[i].Location.Y / (float)_viewBounds.Height;
                else
                    pos[i + 1] = (float)_ls[i].Location.X / (float)_viewBounds.Width;
                color[i + 1] = _ls[i].Color;
            }
            pos[0] = 0.0f; pos[_ls.Count + 1] = 1.0f;
            color[0] = color[1]; color[_ls.Count + 1] = color[_ls.Count];
            ret.Positions = pos;
            ret.Colors = color;
            return ret;
        }

        public Color GetColor(Point p)
        {
            double percent = _orientation == Orientation.Vertical ? (double)p.Y / (double)_viewBounds.Height : (double)p.X / (double)_viewBounds.Width;
            if (_alphaSelect)
            {
                _rgb = Color.FromArgb((int)Math.Round(255 * percent), _rgb);
                _hsb = ColorSpace.HSB.FromRGB(_rgb);
                _lab = ColorSpace.LAB.FromRGB(_rgb);
                return _rgb;
            }
            else
            {
                switch (_colorTableType)
                {
                    case ColorTableType.Red:
                        _rgb = Color.FromArgb(_rgb.A, (int)Math.Round(255 - 255 * percent), _rgb.G, _rgb.B);
                        _hsb = ColorSpace.HSB.FromRGB(_rgb);
                        _lab = ColorSpace.LAB.FromRGB(_rgb);
                        return _rgb;

                    case ColorTableType.Green:
                        _rgb = Color.FromArgb(_rgb.A, _rgb.R, (int)Math.Round(255 - 255 * percent), _rgb.B);
                        _hsb = ColorSpace.HSB.FromRGB(_rgb);
                        _lab = ColorSpace.LAB.FromRGB(_rgb);
                        return _rgb;

                    case ColorTableType.Blue:
                        _rgb = Color.FromArgb(_rgb.A, _rgb.R, _rgb.G, (int)Math.Round(255 - 255 * percent));
                        _hsb = ColorSpace.HSB.FromRGB(_rgb);
                        _lab = ColorSpace.LAB.FromRGB(_rgb);
                        return _rgb;

                    case ColorTableType.Hue:
                        _hsb = ColorSpace.HSB.FromHSB(_hsb.A, 1.0d - percent, _hsb.S, _hsb.B);
                        _rgb = (Color)_hsb;
                        _lab = ColorSpace.LAB.FromRGB(_rgb);
                        return _rgb;

                    case ColorTableType.Saturation:
                        _hsb = ColorSpace.HSB.FromHSB(_hsb.A, _hsb.H, 1.0d - percent, _hsb.B);
                        _rgb = (Color)_hsb;
                        _lab = ColorSpace.LAB.FromRGB(_rgb);
                        return _rgb;

                    case ColorTableType.Brightness:
                        _hsb = ColorSpace.HSB.FromHSB(_hsb.A, _hsb.H, _hsb.S, 1.0d - percent);
                        _rgb = (Color)_hsb;
                        _lab = ColorSpace.LAB.FromRGB(_rgb);
                        return _rgb;

                    case ColorTableType.LabL:
                        _lab = ColorSpace.LAB.FromLAB(_lab.Alpha, (1.0d - percent) * 100, _lab.A, _lab.B);
                        _rgb = (Color)_lab;
                        _hsb = ColorSpace.HSB.FromRGB(_rgb);
                        return _rgb;

                    case ColorTableType.Laba:
                        _lab = ColorSpace.LAB.FromLAB(_lab.Alpha, _lab.L, 127.0d - percent * 255.0d, _lab.B);
                        _rgb = (Color)_lab;
                        _hsb = ColorSpace.HSB.FromRGB(_rgb);
                        return _rgb;

                    case ColorTableType.Labb:
                        _lab = ColorSpace.LAB.FromLAB(_lab.Alpha, _lab.L, _lab.A, 127.0d - percent * 255.0d);
                        _rgb = (Color)_lab;
                        _hsb = ColorSpace.HSB.FromRGB(_rgb);
                        return _rgb;

                    default:
                        throw new NotImplementedException();
                }
            }
        }

        public Point GetPosition()
        {
            double percent;
            if (_alphaSelect)
            {
                percent = (double)_rgb.A / 255.0d;
            }
            else
            {
                switch (_colorTableType)
                {
                    case ColorTableType.Red:
                        percent = 1.0 - (double)_rgb.R / 255.0d;
                        break;

                    case ColorTableType.Green:
                        percent = 1.0 - (double)_rgb.G / 255.0d;
                        break;

                    case ColorTableType.Blue:
                        percent = 1.0 - (double)_rgb.B / 255.0d;
                        break;

                    case ColorTableType.Hue:
                        percent = 1.0 - _hsb.H;
                        break;

                    case ColorTableType.Saturation:
                        percent = 1.0 - _hsb.S;
                        break;

                    case ColorTableType.Brightness:
                        percent = 1.0 - _hsb.B;
                        break;

                    case ColorTableType.LabL:
                        percent = 1.0 - _lab.L / 100.0;
                        break;

                    case ColorTableType.Laba:
                        percent = (127.0 - _lab.A) / 255.0;
                        break;

                    case ColorTableType.Labb:
                        percent = (127.0 - _lab.B) / 255.0;
                        break;

                    default:
                        throw new NotImplementedException();
                }
            }

            if (_orientation == Orientation.Vertical)
            {
                return new Point(0, Convert.ToInt32(percent * (double)_viewBounds.Height + _viewBounds.Top));
            }
            else
            {
                return new Point(Convert.ToInt32(percent * (double)_viewBounds.Width + _viewBounds.Left), 0);
            }
        }

        [Category("ColorSlider"), Description("透明度选择方式")]
        public bool AlphaSelect
        {
            get { return _alphaSelect; }
            set
            {
                if (_alphaSelect != value)
                {
                    _alphaSelect = value;
                    OnColorChanged(EventArgs.Empty);
                    Invalidate();
                }
            }
        }

        [Category("ColorSlider"), Description("背景纹理图像")]
        public Bitmap BackTexture
        {
            get { return _backTexture; }
            set
            {
                _backTexture = value;
                Invalidate();
            }
        }

        [Category("ColorSlider"), Description("定义ColorSlider滑标显示方式")]
        public SliderSide SliderVisible
        {
            get { return _sliderVisible; }
            set
            {
                if (_sliderVisible != value)
                {
                    _sliderVisible = value;
                    CreateViewBounds();
                    Invalidate();
                }
            }
        }

        [Category("ColorSlider"), Description("定义ColorSlider显示的方向")]
        public Orientation Orientation
        {
            get { return _orientation; }
            set
            {
                if (_orientation != value)
                {
                    _orientation = value;
                    CreateViewBounds();
                    CreateBrush();
                    Invalidate();
                }
            }
        }

        [Browsable(false)]
        public List<Slider> Sliders
        {
            get { return _sliders; }
            protected set { _sliders = value; }
        }

        [Category("ColorSlider"), Description("HSB颜色")]
        public ColorSpace.HSB HSB
        {
            get { return _hsb; }
            set
            {
                if (_hsb != value)
                {
                    _hsb = value;
                    _rgb = _hsb.ToRGB();
                    _lab = ColorSpace.RGB2LAB(_rgb);
                    OnColorChanged(EventArgs.Empty);
                    Invalidate();
                }
            }
        }

        [Category("ColorSlider"), Description("RGB颜色")]
        public Color RGB
        {
            get { return _rgb; }
            set
            {
                if (_rgb != value)
                {
                    _rgb = value;
                    _hsb = ColorSpace.HSB.FromRGB(_rgb);
                    _lab = ColorSpace.RGB2LAB(_rgb);
                    OnColorChanged(EventArgs.Empty);
                    Invalidate();
                }
            }
        }

        [Category("ColorSlider"), Description("Lab颜色")]
        public ColorSpace.LAB Lab
        {
            get { return _lab; }
            set
            {
                if (_lab != value)
                {
                    _lab = value;
                    _rgb = _lab.ToRGB();
                    _hsb = ColorSpace.RGB2HSB(_rgb);
                    OnColorChanged(EventArgs.Empty);
                    Invalidate();
                }
            }
        }

        [Category("ColorSlider"), Description("指示是否渐变配制功能，基于RGB颜色空间")]
        public bool SpectrumMaker
        {
            get { return _spectrumMaker; }
            set
            {
                if (_spectrumMaker != value)
                {
                    _alphaSelect = !value;
                    _spectrumMaker = value;
                    if (!_spectrumMaker && _sliders.Count > 1) _sliders.RemoveRange(1, _sliders.Count - 1);
                    Invalidate();
                }
            }
        }

        public ColorTableType ColorTableType
        {
            get { return _colorTableType; }
            set
            {
                if (_colorTableType != value)
                {
                    _colorTableType = value;
                    OnColorChanged(EventArgs.Empty);
                    Invalidate();
                }
            }
        }

        [Browsable(false)]
        public Rectangle ViewBounds
        {
            get { return _viewBounds; }
        }

        [Browsable(false)]
        public Slider ActiveSlider
        {
            get { return _activatedSlider; }
        }

        public Color PickedColor
        {
            get { return _pickedColor; }
            set { _pickedColor = value; }
        }
    }
}