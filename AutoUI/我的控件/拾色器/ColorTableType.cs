﻿namespace ColorPane
{
    /// <summary>
    /// 定义色谱绘制的枚举类型
    /// </summary>
    public enum ColorTableType
    {
        Hue,
        Saturation,
        Brightness,
        Red,
        Green,
        Blue,
        LabL,
        Laba,
        Labb
    }
}