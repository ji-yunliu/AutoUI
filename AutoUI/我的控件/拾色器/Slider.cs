using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.ComponentModel;
using System.Runtime.InteropServices;

namespace ColorPane
{
    /// <summary>
    /// 定义滑标（游标），与ColorSlider关联
    /// </summary>
    public class Slider
    {
        public event EventHandler LocationChanged;

        public event EventHandler ColorChanged;

        private ColorSlider _parent;//ColorSlider父级容器
        private Point _location;
        private Color _color = Color.White;
        private Color _border = Color.FromArgb(116, 114, 106);
        private bool _triangleStyle;//表示是否三角形样式

        public Slider(ColorSlider parent)
        {
            _parent = parent;
            if (_parent != null) _triangleStyle = _parent.SpectrumMaker;
        }

        public Slider()
            : this(null)
        {
        }

        public Point Location
        {
            get { return _location; }
            set
            {
                int pos;
                if (_location != value)
                {
                    if (_parent != null)
                    {
                        if (_parent.Orientation == Orientation.Vertical)
                        {
                            pos = value.Y < _parent.ViewBounds.Y ? _parent.ViewBounds.Y : value.Y > _parent.ViewBounds.Bottom ? _parent.ViewBounds.Bottom : value.Y;
                            _location = new Point(value.X, pos);
                            OnLocationChanged(EventArgs.Empty);
                        }
                        else
                        {
                            pos = value.X < _parent.ViewBounds.X ? _parent.ViewBounds.X : value.X > _parent.ViewBounds.Right ? _parent.ViewBounds.Right : value.X;
                            _location = new Point(pos, value.Y);
                            OnLocationChanged(EventArgs.Empty);
                        }
                        return;
                    }
                    _location = value;
                    OnLocationChanged(EventArgs.Empty);
                }
            }
        }

        public Color Color
        {
            get { return _color; }
            set
            {
                if (_color != value)
                {
                    _color = value;
                    OnColorChanged(EventArgs.Empty);
                }
            }
        }

        public Color Border
        {
            get { return _border; }
            set { _border = value; }
        }

        public bool TriangleStyle
        {
            get { return _triangleStyle; }
            set { _triangleStyle = value; }
        }

        protected virtual void OnLocationChanged(EventArgs e)
        {
            if (LocationChanged != null) LocationChanged(this, e);
        }

        protected virtual void OnColorChanged(EventArgs e)
        {
            if (ColorChanged != null) ColorChanged(this, e);
        }
    }

    public class SliderSorter : IComparer<Slider>
    {
        private Orientation _orientation;

        public SliderSorter(Orientation orientation)
        {
            _orientation = orientation;
        }

        public SliderSorter()
            : this(Orientation.Vertical)
        {
        }

        #region IComparer<Slider> 成员

        public int Compare(Slider x, Slider y)
        {
            if (_orientation == Orientation.Vertical)
            {
                return x.Location.Y - y.Location.Y;
            }
            else
            {
                return x.Location.X - y.Location.X;
            }
        }

        #endregion IComparer<Slider> 成员

        public Orientation Orientation
        {
            get { return _orientation; }
            set { _orientation = value; }
        }
    }

    /// <summary>
    /// 封装滑标事件参数
    /// </summary>
    public class SliderEventArgs : EventArgs
    {
        private Slider _slider;

        public SliderEventArgs(Slider slider)
            : base()
        {
            if (slider == null)
                throw new ArgumentNullException("slider", "构造函数参数 slider 不能为null。");
            _slider = slider;
        }

        public Slider Slider
        {
            get { return _slider; }
        }
    }

    public enum SliderSide
    {
        LeftOrTop = 0,
        RightOrBottom = 1,
        Both = 2
    }

    public delegate void SliderEventHandler(object sender, SliderEventArgs e);
}