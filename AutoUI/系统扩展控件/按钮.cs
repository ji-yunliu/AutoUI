﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AutoUI.系统扩展控件
{
    public partial class 按钮 : Button, ICustomShape
    {
        public 按钮()
        {
            InitializeComponent();
            FlatStyle = FlatStyle.Flat;
        }

        private EnumShapeType _Shape = EnumShapeType.星形;

        [Category("形状设置"), Description("多边形还是N角星"), DisplayName("多边形还是N角星")]
        public EnumShapeType Shape
        {
            get { return _Shape; }
            set
            {
                _Shape = value;
                ResetRegion();
                Invalidate();
            }
        }

        private int _CountLine = 5;

        [Category("形状设置"), Description("几边型或者几角星"), DisplayName("几边型或者几角星")]
        public int CountLine
        {
            get { return _CountLine; }
            set
            {
                if (value > 2)
                {
                    _CountLine = value;
                    ResetRegion();
                    Invalidate();
                }
            }
        }

        private float _StarScale = 0.5f;

        [Category("形状设置"), Description("星形比例大小"), DisplayName("星形比例大小")]
        public float StarScale
        {
            get { return _StarScale; }
            set
            {
                if (value > 0 && value < 1)
                {
                    _StarScale = value;
                    ResetRegion();
                    Invalidate();
                }
            }
        }

        [Category("形状设置"), Description("起始角度"), DisplayName("起始角度")]
        public float AngleOffset
        {
            get { return angleOffset; }
            set
            {
                angleOffset = value;
                ResetRegion();
                Invalidate();
            }
        }

        [Category("功能设置"), Description("是否旋转"), DisplayName("是否旋转")]
        public bool IsAutoRotate
        {
            get { return timer1.Enabled; }
            set
            {
                this.timer1.Enabled = value;
            }
        }

        [Category("功能设置"), Description("旋转速度"), DisplayName("旋转速度")]
        public int RotateSpeed
        {
            get { return timer1.Interval; }
            set
            {
                if (timer1 != null && value > 5)
                {
                    timer1.Interval = value;
                }
            }
        }

        [Category("功能设置"), Description("旋转度数，就是每次旋转多少度，有改变旋转速度的视觉效果"), DisplayName("旋转速度和方向")]
        public int RotateDegrees
        {
            get { return rotateDegrees; }
            set
            {
                rotateDegrees = value;
            }
        }

        private int rotateDegrees;
        private float angleOffset = 0;

        protected override void OnPaint(PaintEventArgs pe)
        {
            base.OnPaint(pe);
        }

        private void ResetRegion()
        {
            var rec = this.ClientRectangle;
            var outlength = Math.Min(rec.Width, rec.Height);
            GraphicsPath path = null;
            switch (Shape)
            {
                case EnumShapeType.正多边形:
                    var ps = Shapes.CreateRegularPolygon(rec.GetCenterPoint(), outlength / 2, CountLine, angleOffset);
                    path = new GraphicsPath();
                    path.AddLines(ps);
                    this.Region = new Region(path);
                    break;

                case EnumShapeType.星形:
                    path = Shapes.CreateStar(rec.GetCenterPoint(), outlength / 2, outlength / 2 * StarScale, CountLine, angleOffset);
                    this.Region = new Region(path);
                    break;

                case EnumShapeType.圆形:
                    path = new GraphicsPath();
                    rec.Inflate(-2, -2);
                    path.AddEllipse(rec);
                    this.Region = new Region(path);
                    break;

                case EnumShapeType.指针:
                    break;

                case EnumShapeType.圆角矩形:

                    rec.Inflate(-2, -2);
                    //rec.Width -= 2;
                    // rec.Height -= 2;
                    path = Shapes.CreateRoundedRectangle(rec, 20);
                    this.Region = new Region(path);
                    break;

                case EnumShapeType.环形刻度:
                    break;

                default:
                    break;
            }

            //path.Dispose();
        }

        protected override void OnSizeChanged(EventArgs e)
        {
            base.OnSizeChanged(e);
            ResetRegion();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (IsAutoRotate)
            {
                AngleOffset = AngleOffset + RotateDegrees;
                if (AngleOffset > 360)
                {
                    AngleOffset = 0;
                }
            }
        }
    }
}