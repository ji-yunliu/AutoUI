﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace AutoUI
{
    public class Common
    {
        /// <summary>
        /// 计算两条直线的交点
        /// </summary>
        /// <param name="lineFirstStar">L1的点1坐标</param>
        /// <param name="lineFirstEnd">L1的点2坐标</param>
        /// <param name="lineSecondStar">L2的点1坐标</param>
        /// <param name="lineSecondEnd">L2的点2坐标</param>
        /// <returns></returns>
        public static PointF GetCrossPoint(PointF lineFirstStar, PointF lineFirstEnd, PointF lineSecondStar, PointF lineSecondEnd)
        {
            return AutoLine.GetCrossPoint(new AutoLine(lineFirstStar, lineFirstEnd), new AutoLine(lineSecondStar, lineSecondEnd));
        }

        /// <summary>
        /// 获取两点的距离
        /// </summary>
        /// <param name="p1"></param>
        /// <param name="p2"></param>
        /// <returns></returns>
        public static float GetDistance(PointF p1, PointF p2)
        {
            return (float)Math.Sqrt((p1.X - p2.X) * (p1.X - p2.X) + (p1.Y - p2.Y) * (p1.Y - p2.Y));
        }

        /// <summary>
        /// 已知圆外一点和圆求切点：如果点在圆内，将返回null
        /// </summary>
        /// <param name="C">圆的圆心</param>
        /// <param name="P">圆外的一点</param>
        /// <param name="r">圆的半径</param>
        /// <returns></returns>
        public static PointF[] GetQieDian(PointF C, PointF P, double r)
        {
            PointF Q1 = new PointF(); PointF Q2 = new PointF(); PointF U = new PointF();
            double distance = Math.Sqrt((P.X - C.X) * (P.X - C.X) + (P.Y - C.Y) * (P.Y - C.Y));
            // 判断是否符合要求 distance<=r 不符合则返回 否则进行运算
            if (distance <= r)
            {
                return null;
            }
            // 点p 到切点的距离
            double length = Math.Sqrt(distance * distance - r * r);

            // 点到圆心的单位向量
            U.X = (float)((C.X - P.X) / distance);
            U.Y = (float)((C.Y - P.Y) / distance);

            //U.X = (float)((P.X - C.X) / distance);
            //U.Y = (float)((P.Y - C.Y) / distance);
            // 计算切线与点心连线的夹角
            double angle = Math.Asin(r / distance);
            //  angle = Math.Asin(r / distance) * 180 / Math.PI;

            // 向正反两个方向旋转单位向量
            Q1.X = (float)(U.X * Math.Cos(angle) - U.Y * Math.Sin(angle));
            Q1.Y = (float)(U.X * Math.Sin(angle) + U.Y * Math.Cos(angle));
            Q2.X = (float)(U.X * Math.Cos(-angle) - U.Y * Math.Sin(-angle));
            Q2.Y = (float)(U.X * Math.Sin(-angle) + U.Y * Math.Cos(-angle));
            //Q1.X = (float)(U.X * Math.Cos(angle * SysConst.Angle) - U.Y * Math.Sin(angle * SysConst.Angle));
            //Q1.Y = (float)(U.X * Math.Sin(angle * SysConst.Angle) + U.Y * Math.Cos(angle * SysConst.Angle));
            //Q2.X = (float)(U.X * Math.Cos(-angle * SysConst.Angle) - U.Y * Math.Sin(-angle * SysConst.Angle));
            //Q2.Y = (float)(U.X * Math.Sin(-angle * SysConst.Angle) + U.Y * Math.Cos(-angle * SysConst.Angle));
            // 得到新座标
            Q1.X = (float)(Q1.X * length + P.X);
            Q1.Y = (float)(Q1.Y * length + P.Y);
            Q2.X = (float)(Q2.X * length + P.X);
            Q2.Y = (float)(Q2.Y * length + P.Y);
            //Q1.x = Q1.x * length+P.x
            // 输出坐标
            //printf("Q1的坐标为：(%.1f,%.1f),Q2的坐标为：(%.1f,%.1f) \n", Q1.x, Q1.y, Q2.x, Q2.y);

            return new PointF[] { Q1, Q2 };
        }
    }
}