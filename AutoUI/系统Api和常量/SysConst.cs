﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace System
{
    public class SysConst
    {
        /// <summary>
        /// 一度等于多少弧度
        /// </summary>
        public const double Angle = 0.017453292519943295;

        /// <summary>
        /// 一弧度等于多少度
        /// </summary>
        public const double hudue = 57.295779513082323402;

        public static StringFormat StringFormat_Center = new StringFormat() { Alignment = StringAlignment.Center, LineAlignment = StringAlignment.Center };
    }
}