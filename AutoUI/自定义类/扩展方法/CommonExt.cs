﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace AutoUI
{
    public static class CommonExt
    {
        public static PointF GetCenterPoint(this Rectangle rec)
        {
            return new PointF(rec.X + rec.Width / 2, rec.Y + rec.Height / 2);
        }
    }
}