﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace System
{
    public static class MathExt
    {
        public static double Sin(float angle)
        {
            return Math.Sin(angle * (SysConst.Angle));
        }
    }
}