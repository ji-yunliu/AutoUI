﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace AutoUI
{
    public static class ColorExt
    {
        public static Color GetRandomColor()
        {
            var random = new Random();
            return Color.FromArgb(255, random.Next(255), random.Next(255), random.Next(255));
        }

        /// <summary>
        /// 颜色加深:通过增加不透明度
        /// </summary>
        /// <param name="color"></param>
        /// <param name="scale"></param>
        /// <returns></returns>
        public static Color Dark(this Color color, byte scale)
        {
            int A = color.A;
            A = A + scale;
            if (A < 0)
            {
                A = 0;
            }
            else if (A > 255)
            {
                A = 255;
            }
            return Color.FromArgb(A, color.R, color.G, color.B);
        }

        /// <summary>
        /// 颜色变淡:通过减小不透明度
        /// </summary>
        /// <param name="color"></param>
        /// <param name="scale"></param>
        /// <returns></returns>
        public static Color Light(this Color color, int scale)
        {
            int A = color.A;
            A = A - scale;
            if (A < 0)
            {
                A = 0;
            }
            else if (A > 255)
            {
                A = 255;
            }
            return Color.FromArgb(A, color.R, color.G, color.B);
        }

        /// <summary>
        /// 颜色加深或者变浅
        /// </summary>
        /// <param name="color"></param>
        /// <param name="scale">-1.0f <= correctionFactor <= 1.0f</param>
        /// <returns></returns>
        public static Color ChangeColor(this Color color, int scale)
        {
            var red = color.R + scale;
            var green = color.G + scale;
            var blue = color.B + scale;

            if (red < 0) red = 0;

            if (red > 255) red = 255;

            if (green < 0) green = 0;

            if (green > 255) green = 255;

            if (blue < 0) blue = 0;

            if (blue > 255) blue = 255;

            return Color.FromArgb(color.A, (int)red, (int)green, (int)blue);
        }
    }
}