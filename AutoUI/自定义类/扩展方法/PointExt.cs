﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace AutoUI
{
    public static class PointExt
    {
        public static CircleF CreateCircleF(this PointF p, float radius = 1)
        {
            return new CircleF(p, radius);
        }

        public static CircleF CreateCircleF(this Point p, float radius = 1)
        {
            return new CircleF(p, radius);
        }
    }
}