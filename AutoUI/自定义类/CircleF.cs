/*
 感谢杨建昌老师设计提供
 */

using AutoUI;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace AutoUI
{
    /// <summary>
    ///表示二维图形－圆形。
    /// </summary>
    /// <remarks>
    ///
    /// </remarks>
    [TypeConverter(typeof(CircleFConvertor))]
    public sealed class CircleF : ICloneable
    {
        #region Fields

        public static CircleF Empty = new CircleF();

        private PointF _pivot;//圆心
        private float _radius;

        #endregion Fields

        //半径

        #region Constructors

        /// <summary>
        /// 根据一个矩形生成一个圆
        /// </summary>
        /// <param name="rectf"></param>
        public CircleF(RectangleF rectf)
        {
            _pivot = GetCirclePivot(rectf);
            _radius = GetCircleRadius(rectf);
        }

        /// <summary>
        /// 根据圆心和半径生成一个圆
        /// </summary>
        /// <param name="pivot"></param>
        /// <param name="radius"></param>
        public CircleF(PointF pivot, float radius)
        {
            _pivot = pivot;
            _radius = radius;
        }

        /// <summary>
        /// 默认构造函数 ,生成半径为1,圆心为(0,0)的单位元
        /// </summary>
        public CircleF()
            : this(new PointF(0, 0), 1)
        {
            // _pivot = new PointF(0, 0);
            //_radius = 1;
        }

        #endregion Constructors

        #region Properties

        [TypeConverter(typeof(PointFConvertor))]
        public PointF Pivot
        {
            get { return _pivot; }
            set { _pivot = value; }
        }

        public float Radius
        {
            get { return _radius; }
            set
            {
                if (value >= 0)
                    _radius = value;
            }
        }

        #endregion Properties

        #region Methods

        /// <summary>
        /// 获取一个矩形的内切正方形
        /// </summary>
        /// <param name="rect"></param>
        /// <returns></returns>
        public static RectangleF GetBounds(RectangleF rect)
        {
            PointF p = GetCirclePivot(rect);
            float d = GetCircleRadius(rect);
            return new RectangleF(p.X - d, p.Y - d, d * 2.0f, d * 2.0f);
        }

        /// <summary>
        /// 获取矩形的中心点
        /// </summary>
        /// <param name="rect"></param>
        /// <returns></returns>
        public static PointF GetCirclePivot(RectangleF rect)
        {
            return new PointF(rect.X + rect.Width / 2.0f, rect.Y + rect.Height / 2.0f);
        }

        /// <summary>
        /// 获取矩形的内切圆的半径
        /// </summary>
        /// <param name="rect"></param>
        /// <returns></returns>
        public static float GetCircleRadius(RectangleF rect)
        {
            return Math.Min(rect.Width / 2.0f, rect.Height / 2.0f);
        }

        public static implicit operator CircleF(RectangleF c)
        {
            return new CircleF(c);
        }

        public static implicit operator Rectangle(CircleF c)
        {
            var rec = c.GetBounds();
            return new Rectangle((int)rec.X, (int)rec.Y, (int)rec.Width, (int)rec.Height);
        }

        public static implicit operator RectangleF(CircleF c)
        {
            return c.GetBounds();
        }

        /// <summary>
        /// 判断两个圆的位置或大小是否不同。
        /// 圆心和半径都相等才true！
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static bool operator !=(CircleF left, CircleF right)
        {
            return !(left == right);
        }

        /// <summary>
        /// 判断两个圆的位置或大小是否不同。
        /// 圆心和半径都相等才true！
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static bool operator ==(CircleF left, CircleF right)
        {
            var x = left._pivot == right._pivot && left._radius == right._radius;
            return left._pivot == right._pivot && left._radius == right._radius;
        }

        /// <summary>
        /// 求此圆的面积
        /// </summary>
        /// <returns></returns>
        public float Area()
        {
            return (float)Math.PI * _radius * _radius;
        }

        /// <summary>
        ///点是否在圆内,在圆内为true
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public bool Contains(float x, float y)
        {
            return Contains(new PointF(x, y));
        }

        /// <summary>
        /// 点是否在圆内,在圆内为true
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public bool Contains(PointF p)
        {
            return Distance(p) <= _radius;
        }

        /// <summary>
        /// 求取两圆的圆心距
        /// </summary>
        public float Distance(CircleF c)
        {
            return Distance(c.Pivot);
        }

        /// <summary>
        /// 求取指定点与该圆圆心的距离
        /// </summary>
        public float Distance(PointF p)
        {
            return Common.GetDistance(p, _pivot);
            // return (float)Math.Sqrt((p.X - _pivot.X) * (p.X - _pivot.X) + (p.Y - _pivot.Y) * (p.Y - _pivot.Y));
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="rect"></param>
        /// <returns></returns>
        public CircleF FromRectangle(RectangleF rect)
        {
            _pivot = GetCirclePivot(rect);
            _radius = GetCircleRadius(rect);
            return this;
        }

        /// <summary>
        /// 获取该圆的外切正方形
        /// </summary>
        /// <returns></returns>
        public RectangleF GetBounds()
        {
            return new RectangleF(_pivot.X - _radius, _pivot.Y - _radius, _radius * 2.0f, _radius * 2.0f);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        /// <summary>
        /// todo:获取该圆的内切切正方形
        /// </summary>
        /// <returns></returns>
        public RectangleF GetInnerBounds()
        {
            throw new NotImplementedException();
            // return new RectangleF(_pivot.X - _radius, _pivot.Y - _radius, _radius * 2.0f, _radius * 2.0f);
        }

        /// <summary>
        /// 按特定值:将此圆放大或者缩小指定量。正放大 负数缩小
        /// </summary>
        /// <param name="d">半径的放大量</param>
        public void Inflate(float d)
        {
            Radius += d;
        }

        /// <summary>
        /// 按比例:将此圆放大或者缩小指定量。正放大 负数缩小( Radius += Radius * d;)
        /// </summary>
        /// <param name="d">半径的放大量,按照比例</param>
        public void InflateScale(float d)
        {
            Radius += Radius * d;
        }

        /// <summary>
        /// 将此圆形的位置调整指定的量。
        /// </summary>
        /// <param name="x">垂直偏移该位置的量。</param>
        /// <param name="y">水平偏移该位置的量。</param>
        public void Offset(float x, float y)
        {
            _pivot.X += x;
            _pivot.Y += y;
        }

        /// <summary>
        /// 将此圆形的位置调整指定的量。
        /// </summary>
        /// <param name="pos"> 偏移该位置的量。 </param>
        public void Offset(System.Drawing.PointF pos)
        {
            Offset(pos.X, pos.Y);
        }

        /// <summary>
        /// 根据角度求对应位置的坐标
        /// </summary>
        /// <param name="sweepAngle" type="float">
        ///     <para>
        ///         极坐标下的角度。（以度为单位。）
        ///     </para>
        /// </param>
        /// <returns>
        ///     A System.Drawing.PointF value...
        /// </returns>
        public PointF PointOnPath(double sweepAngle)
        {
            return new PointF(_pivot.X + _radius * (float)Math.Cos(sweepAngle * SysConst.Angle),
                                             _pivot.Y - _radius * (float)Math.Sin(sweepAngle * SysConst.Angle));
        }

        public override string ToString()
        {
            return string.Format("{{Pivot={0},Radius={1}}}", _pivot, _radius);
        }

        #endregion Methods

        #region ICloneable 成员

        public object Clone()
        {
            return new CircleF(_pivot, _radius);
        }

        #endregion ICloneable 成员
    }
}