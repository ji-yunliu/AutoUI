﻿using System.ComponentModel;
using System.Drawing;

namespace AutoUI
{
    /// <summary>
    /// 路径渐变
    /// </summary>
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public class BackgroundPathGradient
    {
        #region Properties

        /// <summary>
        /// 渐变角度
        /// </summary>
        [Description("渐变色的角度,默认0°,水平")]
        public float Angle { get; set; } = 0;

        [Description("渐变色的颜色")]
        public Color[] Colors { get; set; } = new Color[] { Color.White, Color.LightBlue };

        [Description("渐变色的位置(0-1之间的数)")]
        public float[] Positions { get; set; } = new float[] { 0.5f };

        #endregion Properties
    }
}