﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Design;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;

namespace AutoUI
{
    /// <summary>
    /// 渐变颜色类
    /// </summary>
    public class GradientColor
    {
        private float colorPositon = 0.5f;

        /// <summary>
        /// 渐变过渡色
        /// </summary>
        [DisplayName("颜色")]
        [Editor(typeof(ColorEditorExt), typeof(UITypeEditor))]
        public Color Color { get; set; } = Color.Green;

        /// <summary>
        /// 过渡色的位置,(0-1)
        /// </summary>
        [DisplayName("对应位置")]
        public float ColorPositon
        {
            get { return colorPositon; }
            set
            {
                if (value > 0 && value < 1)
                {
                    colorPositon = value;
                }
            }
        }

        //public static implicit operator ColorBlend(GradientColor c)
        //{
        //    List<Color> colors = new List<Color>();
        //    colors.Add(c.StartColor);
        //    colors.AddRange(GradientColor_.Select(x => x.Color));
        //    colors.Add(EndColor);
        //    List<float> points = new List<float>();
        //    points.Add(0);
        //    points.AddRange(GradientColor_.Select(x => x.ColorPositon));
        //    points.Add(1);

        //    ColorBlend = new ColorBlend()
        //    {
        //        Colors = colors.ToArray(),
        //        Positions = points.ToArray()
        //    };
        //}
    }
}