﻿namespace AutoUI
{
    /// <summary>
    /// 背景
    /// </summary>
    public enum BackGroudTypes
    {
        /// <summary>
        /// 系统默认
        /// </summary>
        None,

        /// <summary>
        /// 线性渐变背景
        /// </summary>
        LinearGradient,

        /// <summary>
        /// 路径渐变背景
        /// </summary>
        PathGradient
    }
}