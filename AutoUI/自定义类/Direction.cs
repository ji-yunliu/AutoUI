﻿namespace AutoUI
{
    /// <summary>
    /// 管道转向方向
    /// </summary>
    public enum Direction
    {
        Up,
        Down,
        Left,
        Right,
        None
    }
}