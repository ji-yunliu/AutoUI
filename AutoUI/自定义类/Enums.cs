﻿using System.ComponentModel;

namespace AutoUI
{
    internal class Enums
    {
    }

    /// <summary>
    /// 定义一个枚举，表示拖动方向,用于在为无边框窗体增加改变大小功能的时候
    /// </summary>
    public enum MouseDirection
    {
        /// <summary>
        /// 水平方向拖动，只改变窗体的宽度
        /// </summary>
        Herizontal,

        /// <summary>
        /// 垂直方向拖动，只改变窗体的高度
        /// </summary>
        Vertical,

        /// <summary>
        /// 倾斜方向，同时改变窗体的宽度和高度
        /// </summary>
        Declining,

        /// <summary>
        /// 不做标志，即不拖动窗体改变大小
        /// </summary>
        None
    }

    #region 输入类型

    public enum _srlx输入类型
    {
        /// <summary>
        /// 不控制输入
        /// </summary>
        [Description("不控制输入")]
        不控制输入 = 1,

        /// <summary>
        /// 任意数字
        /// </summary>
        [Description("任意数字")]
        任意数字 = 2,

        /// <summary>
        /// 非负数
        /// </summary>
        [Description("非负数")]
        非负数 = 4,

        /// <summary>
        /// 正数
        /// </summary>
        [Description("正数")]
        正数 = 8,

        /// <summary>
        /// 整数
        /// </summary>
        [Description("整数")]
        整数 = 16,

        /// <summary>
        /// 非负整数
        /// </summary>
        [Description("非负整数")]
        非负整数 = 32,

        /// <summary>
        /// 正则验证
        /// </summary>
        [Description("正则验证")]
        正则验证 = 64
    }

    #endregion 输入类型

    #region 动画类型

    /// <summary>
    /// 缓动函数动画类型
    /// </summary>
    [Description("缓动函数动画类型")]
    public enum EasingFunctionTypes
    {
        /// <summary>
        ///
        /// </summary>
        _ys匀速运动,

        /// <summary>
        ///平稳缓慢上升,越来越快一直到1.0;
        /// </summary>
        _bs变速先缓后陡,

        /// <summary>
        ///快速上升,然后平稳一段时间一直到1.0;
        /// </summary>
        _bs变速先陡后缓,

        /// <summary>
        ///先缓慢上升,越来越快,最后又缓慢一直到1.0;
        /// </summary>
        _bs变速慢快慢,

        /// <summary>
        ///缓慢下降-0.37后开始快速上升到1.0;
        /// </summary>
        _bs变速超低极限后恢复,

        /// <summary>
        /// 快速上升到1.37,然后缓慢回到1
        /// </summary>
        _bs变速超高极限后恢复,

        /// <summary>
        /// 先降低-0.18,后快速上升到1.18,最后回到1
        /// </summary>
        _bs变速超高低限后恢复,

        /// <summary>
        /// 正常弹跳,越到高点速度越慢
        /// 弹跳3次半  0.12   0.24   0.48   1.0
        /// </summary>
        _tt弹跳越来越高,

        /// <summary>
        ///反着弹跳 ,开始到1,最后还是1
        /// 弹跳3次半  1.0   0.48   0.76   0.87 1.0
        /// </summary>
        _tt弹跳越来越低,

        /// <summary>
        /// 两种弹跳综合,先in方式到0.5 后out方式到1综合
        /// </summary>
        _tt弹跳二合一,

        /// <summary>
        /// 振幅越来越大的Sin曲线图(中心轴为0)
        /// </summary>
        _Sin曲线振幅变大,

        /// <summary>
        /// 振幅越来越小的Sin曲线图,开始上升到1.6,最后平稳到1(中心轴为1)
        /// </summary>
        _Sin曲线振幅变小,

        /// <summary>
        /// 两种Sin的综合
        /// </summary>
        _Sin曲线综合,

        /// <summary>
        /// 向上的抛物线(在高点是平稳的),但是最后急剧回0
        /// </summary>
        _pwx抛物线1,

        /// <summary>
        /// 抛物线,但是在高点的时候是突然下降的,最后急剧回0
        /// </summary>
        _pwx抛物线2
    }

    #endregion 动画类型
}