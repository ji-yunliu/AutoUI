﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace AutoUI
{
    public struct Pc
    {
        private Color start;

        public Color Start
        {
            get { return start; }
            set { start = value; }
        }

        private Color end;

        public Color End
        {
            get { return end; }
            set { end = value; }
        }

        public Pc(Color x, Color y)
        {
            this.start = x;
            this.end = y;
        }
    }
}