﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace AutoUI
{
    //  [Serializable]
    //  [ComVisible(true)]
    public class Pcolor
    {
        private Color start = Color.Red;

        public Pcolor()
        {
        }

        public Pcolor(Color start, Color end)
        {
            Start = start;
            End = end;
        }

        public Color Start
        {
            get
            {
                return start;
            }

            set
            {
                start = value;
            }
        }

        public Color End { get; set; } = Color.Yellow;
    }
}