﻿using System.ComponentModel;
using System.Drawing;

namespace AutoUI
{
    /// <summary>
    /// 线性渐变
    /// </summary>
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public class BackgroundLinearGradient
    {
        #region Properties

        /// <summary>
        /// 渐变角度
        /// </summary>
        [Category("背景色"), Description("渐变色的角度,默认0°,水平")]
        public float Angle { get; set; } = 0;

        private Color _StartColor = Color.White;

        /// <summary>
        /// 渐变的起始颜色
        /// </summary>
        [Category("背景色"), Description("渐变的起始颜色")]
        public Color StartColor
        {
            get { return _StartColor; }
            set
            {
                _StartColor = value;
            }
        }

        private Color _EndColor = Color.Black;

        /// <summary>
        /// 渐变的结束颜色
        /// </summary>
        [Category("背景色"), Description("渐变的结束颜色")]
        public Color EndColor
        {
            get { return _EndColor; }
            set
            {
                _EndColor = value;
            }
        }

        /// <summary>
        /// 渐变颜色素组
        /// </summary>
        [Category("背景色"), Description("渐变色的过渡颜色")]
        public Color[] TransitionColors { get; set; } = new Color[] { };

        /// <summary>
        /// 渐变点，TransitionColors
        /// </summary>
        [Category("背景色"), Description("渐变色的过渡位置(0-1之间的数)")]
        public float[] TransitionPositions { get; set; } = new float[] { };

        #endregion Properties
    }
}