﻿namespace AutoUI
{
    /// <summary>
    /// 绘制边框还是填充边框
    /// </summary>
    public enum EnumDrawOrFill
    {
        /// <summary>
        /// 只绘制边框
        /// </summary>
        OnlyDraw,

        /// <summary>
        /// 只填充
        /// </summary>
        OnlyFill,

        /// <summary>
        /// 同时绘制边框和填充
        /// </summary>
        DrawAndFill
    }
}