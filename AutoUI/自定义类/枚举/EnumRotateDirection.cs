﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoUI
{
    /// <summary>
    /// 旋转方向
    /// </summary>
    public enum EnumRotateDirection
    {
        /// <summary>
        /// 停止
        /// </summary>
        Stop,

        /// <summary>
        /// 正转 顺时针
        /// </summary>
        Forward,

        /// <summary>
        /// 反转 逆时针
        /// </summary>
        Reversal
    }
}