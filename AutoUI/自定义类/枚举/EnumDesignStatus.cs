﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoUI
{
    /// <summary>
    /// 正在设计/修改/设计完成
    /// </summary>
    public enum EnumDesignStatus
    {
        /// <summary>
        /// 修改
        /// </summary>
        Change,

        /// <summary>
        /// 新增设计
        /// </summary>
        Designing,

        /// <summary>
        /// 设计完成
        /// </summary>
        Designed
    }
}