﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoUI
{
    /// <summary>
    /// 各种形状名称
    /// </summary>
    public enum EnumShapeType
    {
        正多边形,
        星形,
        指针,
        圆形,
        圆角矩形,
        环形刻度
    }
}