﻿using AutoUI;
using System.ComponentModel;

namespace System.Drawing.Drawing2D
{
    public class CircleFConvertor : TypeConverter
    {
        public CircleFConvertor() : base()
        { }

        public override bool GetPropertiesSupported(ITypeDescriptorContext context)
        {
            return true;
        }

        public override PropertyDescriptorCollection GetProperties(ITypeDescriptorContext context, object value, Attribute[] attributes)
        {
            return TypeDescriptor.GetProperties(typeof(CircleF), attributes).Sort(new string[] { "Radius", "Pivot" });
        }

        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            if (sourceType == typeof(string))
                return true;
            else
                return base.CanConvertFrom(context, sourceType);
        }

        public override object ConvertFrom(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value)
        {
            if (value is string)
            {
                string[] ti = ((string)value).Split(',');
                float x = float.Parse(ti[0].Substring(1));
                float y = float.Parse(ti[1].Substring(0, ti[1].Length - 1));
                float r = float.Parse(ti[2]);
                return new CircleF(new PointF(x, y), r);
            }
            return base.ConvertFrom(context, culture, value);
        }

        public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
        {
            if (destinationType == typeof(string))
                return true;
            return base.CanConvertTo(context, destinationType);
        }

        public override object ConvertTo(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value, Type destinationType)
        {
            if (value is CircleF)
            {
                if (destinationType == typeof(string))
                {
                    CircleF c = value as CircleF;
                    return string.Format("({0},{1}),{2}", c.Pivot.X, c.Pivot.Y, c.Radius);
                }
            }
            return base.ConvertTo(context, culture, value, destinationType);
        }
    }
}