﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace AutoUI
{
    public interface ICustomShape
    {
        /// <summary>
        /// 外观类型
        /// </summary>
        EnumShapeType Shape { get; set; }

        /// <summary>
        /// 有多少边
        /// </summary>
        int CountLine { get; set; }

        /// <summary>
        /// 为星形的时候，星形的大小比例（0-1）
        /// </summary>
        [Category("参数设置"), Description("是否激活流动效果"), DefaultValue(false), DisplayName("是否激活")]
        float StarScale { get; set; }

        /// <summary>
        /// 起始角度
        /// </summary>
        float AngleOffset { get; set; }

        bool IsAutoRotate { get; set; }

        int RotateSpeed { get; set; }
    }
}