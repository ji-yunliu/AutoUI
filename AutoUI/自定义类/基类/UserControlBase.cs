﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AutoUI
{
    [ToolboxItem(false)]
    public partial class UserControlBase : UserControl
    {
        public UserControlBase()
        {
            InitializeComponent();
            Init();
        }

        private void Init()
        {
            Color[] colors = new Color[background.TransitionColors.Length + 2];
            float[] floats = new float[background.TransitionPositions.Length + 2];
            colors[0] = background.StartColor;
            colors[colors.Length - 1] = background.EndColor;
            floats[0] = 0;
            floats[colors.Length - 1] = 1;
            for (int i = 1; i < colors.Length - 1; i++)
            {
                colors[i] = background.TransitionColors[i - 1];
                floats[i] = background.TransitionPositions[i - 1];
            }
            ColorBlend.Colors = colors;
            ColorBlend.Positions = floats;
            lb = new LinearGradientBrush(ClientRectangle, Background.StartColor, Background.EndColor, Background.Angle);
        }

        private ColorBlend ColorBlend = new ColorBlend();

        private LinearGradientBrush lb;

        #region Fields

        private BackgroundLinearGradient background = new BackgroundLinearGradient();
        private bool canMove;

        private bool isdown = false;

        private Point startpoint;

        private Point startpoint2;

        #endregion Fields

        #region Properties

        [DisplayName("渐变背景色属性")]
        public BackgroundLinearGradient Background
        {
            get { return background; }
            set
            {
                background = value;
                Init();
                this.Invalidate();
            }
        }

        [Description("是否能过通过鼠标重新定位本控件的位置")]
        public bool CanMove
        {
            get { return canMove; }
            set { canMove = value; }
        }

        #endregion Properties

        #region Methods

        protected override void OnMouseDown(MouseEventArgs e)
        {
            base.OnMouseDown(e);
            if (e.Button == MouseButtons.Left)
            {
                isdown = true;
                startpoint = MousePosition;
                startpoint2 = this.Location;
            }
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);
            if (isdown && CanMove == true)
            {
                var offsetx = MousePosition.X - startpoint.X;
                var offsety = MousePosition.Y - startpoint.Y;
                Console.WriteLine("x:" + offsetx + "y:" + offsety);
                this.Location = new Point(startpoint2.X + offsetx, startpoint2.Y + offsety);
            }
        }

        protected override void OnMouseUp(MouseEventArgs e)
        {
            base.OnMouseUp(e);
            isdown = false;
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            //  OnPaintBackground1(e);
        }

        protected override void OnPaintBackground(PaintEventArgs pevent)
        {
            base.OnPaintBackground(pevent);
            DrawBackground(pevent.Graphics);
        }

        private void DrawBackground(Graphics g)
        {
            // lb.InterpolationColors = ColorBlend;
            //lb.InterpolationColors = new ColorBlend()
            //{
            //    Colors = new Color[] { Background.StartColor, Background.EndColor },
            //    Positions = new float[] { 0, 1 }
            //};
            g.FillRectangle(lb, 0, 0, this.Width, this.Height);
        }

        #endregion Methods
    }
}