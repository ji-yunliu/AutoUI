﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace AutoUI
{
    [Designer(typeof(ControlBaseDesigner)), ToolboxItem(false)]
    public partial class ControlBase : Control
    {
        #region Fields

        private Point _BackGroundCenterPoint;
        private bool _DesignerMove;
        private float angle = 0;
        private bool canMove;

        /// <summary>
        /// 渐变色数组
        /// </summary>
        private ColorBlend colorBlend = new ColorBlend() { Positions = new float[] { 0f, 1f }, Colors = new Color[] { Color.Black, Color.White } };

        private GraphicsPath gp;

        private bool isdown = false;

        /// <summary>
        /// 线性渐变画刷
        /// </summary>
        private LinearGradientBrush lb;

        /// <summary>
        /// 路径渐变画刷
        /// </summary>
        private PathGradientBrush pb;

        private Point startpoint;

        private Point startpoint2;

        private Color[] transitionColors = new Color[] { };

        private float[] transitionPositions = new float[] { };

        #endregion Fields

        #region Constructors

        public ControlBase()
        {
            SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.OptimizedDoubleBuffer | ControlStyles.ResizeRedraw, true);
            BackGroundCenterPoint = new Point(this.Width / 2, this.Height / 2);

            this.SizeChanged += ControlBase_SizeChanged;
        }

        #endregion Constructors

        #region Properties

        private BackGroudTypes _BackGroudType = BackGroudTypes.None;

        /// <summary>
        /// 渐变角度
        /// </summary>
        [Category("背景色"), Description("渐变色的角度,默认0°,水平")]
        public float Angle
        {
            get { return angle; }
            set
            {
                angle = value;
                Invalidate();
            }
        }

        [Category("背景色"), Description("描述")]
        public BackGroudTypes BackGroudType
        {
            get { return _BackGroudType; }
            set
            {
                _BackGroudType = value;
                this.Invalidate();
            }
        }

        [Category("背景色"), Description("渐变中心点")]
        public Point BackGroundCenterPoint
        {
            get { return _BackGroundCenterPoint; }
            set
            {
                _BackGroundCenterPoint = value;
                this.Invalidate();
            }
        }

        /// <summary>
        /// 是否能过通过鼠标重新定位本控件的位置
        /// </summary>
        [Description("是否能过通过鼠标重新定位本控件的位置")]
        public bool CanMove
        {
            get { return canMove; }
            set { canMove = value; }
        }

        [Category("背景色"), Description("渐变色:颜色和个数必须相等切大于1;且第一为0,最后为1")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public ColorBlend ColorBlend
        {
            get { return colorBlend; }
            set
            {
                colorBlend = value;
                Invalidate();
            }
        }

        /// <summary>
        /// 设计时能否移动
        /// </summary>
        [Browsable(false), Category("自定义属性"), Description("描述")]
        public bool DesignerMove
        {
            get { return _DesignerMove; }
            set
            {
                _DesignerMove = value;
                this.Invalidate();
            }
        }

        ///// <summary>
        ///// 渐变的结束颜色
        ///// </summary>
        //[Category("背景色"), Description("渐变的结束颜色")]
        //public Color EndColor
        //{
        //    get { return _EndColor; }
        //    set
        //    {
        //        _EndColor = value;
        //        Invalidate();
        //    }
        //}

        ///// <summary>
        ///// 渐变的起始颜色
        ///// </summary>
        //[Category("背景色"), Description("渐变的起始颜色")]
        //public Color StartColor
        //{
        //    get { return _StartColor; }
        //    set
        //    {
        //        _StartColor = value;
        //        Invalidate();
        //    }
        //}

        ///// <summary>
        ///// 渐变颜色素组
        ///// </summary>
        //[Category("背景色"), Description("渐变色的过渡颜色")]
        //public Color[] TransitionColors
        //{
        //    get => transitionColors; set
        //    {
        //        transitionColors = value;
        //        Invalidate();
        //    }
        //}

        ///// <summary>
        ///// 渐变点，TransitionColors
        ///// </summary>
        //[Category("背景色"), Description("渐变色的过渡位置(0-1之间的数)")]
        //public float[] TransitionPositions
        //{
        //    get => transitionPositions; set
        //    {
        //        transitionPositions = value;
        //        Invalidate();
        //    }
        //}

        #endregion Properties

        #region Methods

        protected override void OnMouseDown(MouseEventArgs e)
        {
            base.OnMouseDown(e);
            if (e.Button == MouseButtons.Left)
            {
                isdown = true;
                startpoint = MousePosition;
                startpoint2 = this.Location;
            }
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);

            if (isdown && CanMove == true)
            {
                var offsetx = MousePosition.X - startpoint.X;
                var offsety = MousePosition.Y - startpoint.Y;
                Console.WriteLine("x:" + offsetx + "y:" + offsety);
                this.Location = new Point(startpoint2.X + offsetx, startpoint2.Y + offsety);
            }
        }

        protected override void OnMouseUp(MouseEventArgs e)
        {
            base.OnMouseUp(e);
            isdown = false;
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);

            // DrawBackground(e.Graphics);
        }

        protected override void OnPaintBackground(PaintEventArgs pevent)
        {
            base.OnPaintBackground(pevent);
            DrawBackground(pevent.Graphics);
        }

        private void ControlBase_SizeChanged(object sender, EventArgs e)
        {
            Init();
            this.Invalidate();
        }

        private void DrawBackground(Graphics g)
        {
            Init();
            switch (BackGroudType)
            {
                case BackGroudTypes.LinearGradient:
                    g.FillRectangle(lb, 0, 0, this.Width, this.Height);
                    break;

                case BackGroudTypes.PathGradient:
                    g.FillPath(this.pb, this.gp);
                    break;

                case BackGroudTypes.None:
                    break;
            }
        }

        public void Init()
        {
            bool CheckColorBlend(ColorBlend cb)
            {
                return cb.Colors.Length == cb.Positions.Length && cb.Colors.Length > 1 && cb.Positions[0] == 0 && cb.Positions[cb.Positions.Length - 1] == 1;
            }

            switch (BackGroudType)
            {
                case BackGroudTypes.None:
                    if (lb != null) lb.Dispose();
                    if (pb != null) pb.Dispose();
                    break;

                case BackGroudTypes.LinearGradient:
                    lb = new LinearGradientBrush(ClientRectangle, colorBlend.Colors[0], colorBlend.Colors[1], Angle);
                    if (CheckColorBlend(colorBlend))
                    {
                        lb.InterpolationColors = colorBlend;
                    }

                    if (pb != null) pb.Dispose();
                    break;

                case BackGroudTypes.PathGradient:
                    if (lb != null) lb.Dispose();
                    gp = new GraphicsPath();
                    gp.AddRectangle(this.ClientRectangle);
                    pb = new PathGradientBrush(gp);
                    if (CheckColorBlend(colorBlend))
                    {
                        pb.InterpolationColors = colorBlend;
                        pb.CenterPoint = BackGroundCenterPoint;
                        // pb.CenterColor = StartColor;
                    }
                    break;
            }

            //
            //
            //if (TransitionColors.Length > 0 && TransitionPositions.Length > 0)
            //{
            //    Color[] colors = new Color[TransitionColors.Length + 2];
            //    float[] floats = new float[TransitionPositions.Length + 2];
            //    colors[0] = StartColor;
            //    colors[colors.Length - 1] = EndColor;
            //    floats[0] = 0;
            //    floats[colors.Length - 1] = 1;
            //    for (int i = 1; i < colors.Length - 1; i++)
            //    {
            //        colors[i] = TransitionColors[i - 1];
            //        floats[i] = TransitionPositions[i - 1];
            //    }
            //    ColorBlend.Colors = colors;
            //    ColorBlend.Positions = floats;

            //    lb.InterpolationColors = ColorBlend;
            //    pb.InterpolationColors = ColorBlend;
            //}
        }

        #endregion Methods
    }
}