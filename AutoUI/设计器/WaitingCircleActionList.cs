﻿using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.ComponentModel.Design;

namespace System.Windows.Forms.Design
{
    public class WaitingCircleActionList : DesignerActionList
    {
        private 环形等待进度条 _wc;

        public WaitingCircleActionList(IComponent comp)
            : base(comp)
        {
            _wc = (环形等待进度条)comp;
        }

        public Color SpokeColor
        {
            get { return _wc.SpokeColor; }
            set { TypeDescriptor.GetProperties(_wc)["SpokeColor"].SetValue(_wc, value); }
        }

        public Color HotSpokeColor
        {
            get { return _wc.HotSpokeColor; }
            set { TypeDescriptor.GetProperties(_wc)["HotSpokeColor"].SetValue(_wc, value); }
        }

        public LineCap StartCap
        {
            get { return _wc.StartCap; }
            set { TypeDescriptor.GetProperties(_wc)["StartCap"].SetValue(_wc, value); }
        }

        public LineCap EndCap
        {
            get { return _wc.EndCap; }
            set { TypeDescriptor.GetProperties(_wc)["EndCap"].SetValue(_wc, value); }
        }

        public override DesignerActionItemCollection GetSortedActionItems()
        {
            DesignerActionItemCollection items = new DesignerActionItemCollection();
            items.Add(new DesignerActionHeaderItem("辐条颜色"));
            items.Add(new DesignerActionPropertyItem("SpokeColor", "辐条颜色", "辐条颜色"));
            items.Add(new DesignerActionPropertyItem("HotSpokeColor", "活动辐条颜色", "辐条颜色"));

            items.Add(new DesignerActionHeaderItem("辐条样式"));
            items.Add(new DesignerActionPropertyItem("StartCap", "辐条头线帽", "辐条样式"));
            items.Add(new DesignerActionPropertyItem("EndCap", "辐条尾线帽", "辐条样式"));

            items.Add(new DesignerActionHeaderItem("关于"));
            items.Add(new DesignerActionMethodItem(this, "OnSendMail", "发送邮件:mirakia@hotmail.com", "关于"));
            items.Add(new DesignerActionTextItem("版权所有(C)", "关于"));

            return items;
        }

        private void OnSendMail()
        {
            System.Diagnostics.Process.Start("mailto:mirakia@hotmail.com");
        }
    }
}