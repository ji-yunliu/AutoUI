﻿using System.Drawing;
using System.Windows.Forms.Design.Behavior;

namespace System.Windows.Forms.Design
{
    public partial class WaitingCircleGlyph
    {
        internal class WaitingCircleBehavior : Behavior.Behavior
        {
            public override bool OnMouseDown(Glyph g, MouseButtons button, Drawing.Point mouseLoc)
            {
                WaitingCircleGlyph wcg = g as WaitingCircleGlyph;
                if (null != wcg && button == MouseButtons.Left)
                {
                    wcg._fill = !wcg._fill;
                    wcg._adorner.Invalidate();
                }
                return true;
            }
        }
    }
}