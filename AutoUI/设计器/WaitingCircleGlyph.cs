﻿using AutoUI;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms.Design.Behavior;

namespace System.Windows.Forms.Design
{
    public partial class WaitingCircleGlyph : Glyph
    {
        internal BehaviorService _bsService;
        internal 环形等待进度条 _waitCircle;
        internal Adorner _adorner;
        internal bool _fill = true;

        public WaitingCircleGlyph(BehaviorService service, 环形等待进度条 control, Adorner adorner)
            : base(new WaitingCircleBehavior())
        {
            _bsService = service;
            _waitCircle = control;
            _adorner = adorner;
        }

        public new CircleF Bounds
        {
            get
            {
                Drawing.Point loc = _bsService.ControlToAdornerWindow(_waitCircle);
                CircleF c = new CircleF(_waitCircle.ClientRectangle);
                c.Radius = _waitCircle.InnerRadius;
                c.Pivot = new PointF(c.Pivot.X + loc.X, c.Pivot.Y + loc.Y);
                return c;
            }
        }

        public override void Paint(PaintEventArgs pe)
        {
            pe.Graphics.SmoothingMode = SmoothingMode.AntiAlias;
            if (_fill)
            {
                using (SolidBrush sb = new SolidBrush(Color.FromArgb(255 - _waitCircle.SpokeColor.R, 255 - _waitCircle.SpokeColor.G, 255 - _waitCircle.SpokeColor.B)))
                {
                    pe.Graphics.FillEllipse(sb, Bounds);
                    pe.Graphics.DrawLine(Pens.Red, 0, 0, 0, 100);
                }
            }
            else
                using (Pen p = new Pen(Color.FromArgb(255 - _waitCircle.SpokeColor.R, 255 - _waitCircle.SpokeColor.G, 255 - _waitCircle.SpokeColor.B), 3.0f))
                {
                    pe.Graphics.DrawEllipse(p, Bounds);
                    pe.Graphics.DrawLine(Pens.Blue, 0, 0, 0, 100);
                }
        }

        public override Cursor GetHitTest(Drawing.Point p)
        {
            if (Bounds.Contains(p))
                return Cursors.Hand;
            else
                return null;
        }
    }
}