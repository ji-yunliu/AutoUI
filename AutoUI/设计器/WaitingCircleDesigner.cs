using System;
using System.ComponentModel;
using System.Drawing.Design;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using System.ComponentModel.Design;
using System.Collections;
using System.Windows.Forms.Design.Behavior;
using AutoUI;

namespace System.Windows.Forms.Design
{
    public class WaitingCircleDesigner : ControlDesigner
    {
        private DesignerVerbCollection _verbs;
        private ISelectionService _iss;//选取服务
        private static DesignerVerb _dvAntialias;//"平滑WaitingCircle辐条边缘"菜单项

        private DesignerActionListCollection _actionlist;
        private BehaviorService _bs;
        private Adorner _adorner;

        public WaitingCircleDesigner()
            : base()
        {
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            if (disposing)
            {
                _dvAntialias = null;
                if (_iss != null)
                    _iss.SelectionChanged -= new EventHandler(OnComponentSelectionChanged);
                _iss = null;
                _verbs = null;
                _bs.Adorners.Remove(_adorner);
                _bs = null;
                _adorner = null;
            }
        }

        public override void Initialize(IComponent component)
        {
            base.Initialize(component);
            _iss = (ISelectionService)GetService(typeof(ISelectionService));
            _iss.SelectionChanged += new EventHandler(OnComponentSelectionChanged);
            _bs = (BehaviorService)GetService(typeof(BehaviorService));
            _adorner = new Adorner();
            _bs.Adorners.Add(_adorner);
            _adorner.Glyphs.Add(new WaitingCircleGlyph(_bs, (环形等待进度条)Control, _adorner));
        }

        public override DesignerVerbCollection Verbs
        {
            get
            {
                if (_verbs == null)
                {
                    _verbs = new DesignerVerbCollection();
                    _verbs.Add(new DesignerVerb("激活/取消激活", new EventHandler(OnActivateCircle)));
                    _verbs.Add(new DesignerVerb("设计器...", new EventHandler(OnShowDesigner)));
                }
                return _verbs;
            }
        }

        public override DesignerActionListCollection ActionLists
        {
            get
            {
                if (_actionlist == null)
                {
                    _actionlist = new DesignerActionListCollection();
                    _actionlist.Add(new WaitingCircleActionList(Component));
                }
                return _actionlist;
            }
        }

        private void OnComponentSelectionChanged(object sender, EventArgs e)
        {
            if (_dvAntialias == null)
                _dvAntialias = new DesignerVerb("平滑WaitingCircle辐条边缘", new EventHandler(OnAntialias));
            IMenuCommandService imcs = (IMenuCommandService)GetService(typeof(IMenuCommandService));
            if (!imcs.Verbs.Contains(_dvAntialias)) imcs.AddVerb(_dvAntialias);
            bool all = _iss.SelectionCount > 0;//选择控件都是WaitingCircle
            foreach (IComponent comp in _iss.GetSelectedComponents())
            {
                if (!(comp is 环形等待进度条))
                {
                    all = false;
                    break;
                }
            }
            if (all)
            {
                _dvAntialias.Visible = true;
                _dvAntialias.Checked = ((环形等待进度条)_iss.PrimarySelection).Antialias;
            }
            else
                _dvAntialias.Visible = false;
            if (object.ReferenceEquals(_iss.PrimarySelection, Component))
                _adorner.Enabled = true;
            else
                _adorner.Enabled = false;
        }

        private void OnActivateCircle(object sender, System.EventArgs e)
        {
            环形等待进度条 wc = Control as 环形等待进度条;
            GetPropertyByName("Activate").SetValue(wc, !wc.Activate);
            //if (wc != null) wc.Activate = !wc.Activate;
        }

        /// <summary>
        /// 获取正在设计的控件实例的某一个属性
        /// </summary>
        /// <param name="propName"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentException"></exception>
        private PropertyDescriptor GetPropertyByName(String propName)
        {
            PropertyDescriptor prop;
            prop = TypeDescriptor.GetProperties(Control)[propName];
            if (null == prop)
                throw new ArgumentException("组件未定义该属性。", propName);
            else
                return prop;
        }

        private void OnShowDesigner(object sender, System.EventArgs e)
        {
            WaitingCircleDesignForm wcdf = new WaitingCircleDesignForm();
            环形等待进度条 wc = Control as 环形等待进度条;
            if (wc != null)
            {
                wcdf.waitingCircle1.Activate = wc.Activate;
                wcdf.waitingCircle1.Speed = wc.Speed;
                wcdf.waitingCircle1.ColockWise = wc.ColockWise;
                wcdf.waitingCircle1.NumberOfSpokes = wc.NumberOfSpokes;
            }
            if (wcdf.ShowDialog() == DialogResult.OK)
            {
                GetPropertyByName("Activate").SetValue(Control, wcdf.waitingCircle1.Activate);
                GetPropertyByName("Speed").SetValue(Control, wcdf.waitingCircle1.Speed);
                GetPropertyByName("ColockWise").SetValue(Control, wcdf.waitingCircle1.ColockWise);
                GetPropertyByName("NumberOfSpokes").SetValue(Control, wcdf.waitingCircle1.NumberOfSpokes);
            }
        }

        private void OnAntialias(object sender, EventArgs e)
        {
            环形等待进度条 wc = (环形等待进度条)_iss.PrimarySelection;
            TypeDescriptor.GetProperties(wc)["Antialias"].SetValue(wc, !wc.Antialias);
            _dvAntialias.Checked = wc.Antialias;
            foreach (IComponent comp in _iss.GetSelectedComponents())
            {
                if (comp != wc)
                    TypeDescriptor.GetProperties(comp)["Antialias"].SetValue(comp, wc.Antialias);
            }
        }

        public override IList SnapLines
        {
            get
            {
                IList lines = new ArrayList();
                CircleF bounds = new CircleF(Control.ClientRectangle);
                bounds.Inflate(-5);
                SnapLine left = new SnapLine(SnapLineType.Left, (int)bounds.GetBounds().Left);
                SnapLine right = new SnapLine(SnapLineType.Right, (int)bounds.GetBounds().Right);
                SnapLine top = new SnapLine(SnapLineType.Top, (int)bounds.GetBounds().Top);
                SnapLine bot = new SnapLine(SnapLineType.Bottom, (int)bounds.GetBounds().Bottom);
                lines.Add(left);
                lines.Add(right);
                lines.Add(top);
                lines.Add(bot);
                CircleF inner = new CircleF(bounds.Pivot, ((环形等待进度条)Control).InnerRadius);
                left = new SnapLine(SnapLineType.Left, (int)inner.GetBounds().Left);
                right = new SnapLine(SnapLineType.Right, (int)inner.GetBounds().Right);
                top = new SnapLine(SnapLineType.Top, (int)inner.GetBounds().Top);
                bot = new SnapLine(SnapLineType.Bottom, (int)inner.GetBounds().Bottom);
                lines.Add(left);
                lines.Add(right);
                lines.Add(top);
                lines.Add(bot);
                foreach (object o in base.SnapLines)
                {
                    lines.Add(o);
                }
                return lines;
            }
        }

        public override SelectionRules SelectionRules
        {
            get
            {
                SelectionRules sr = base.SelectionRules;
                if (!((环形等待进度条)Control).Activate)
                    sr |= SelectionRules.Moveable;
                else
                    sr &= ~SelectionRules.Moveable;
                return sr;
            }
        }
    }
}