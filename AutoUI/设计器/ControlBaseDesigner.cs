﻿using System;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Forms.Design;

namespace AutoUI
{
    public class ControlBaseDesigner : ControlDesigner
    {
        private DesignerVerbCollection _verbs;

        /// <summary>
        /// 添加右键菜单
        /// </summary>
        public override DesignerVerbCollection Verbs
        {
            get
            {
                if (_verbs == null)
                {
                    _verbs = new DesignerVerbCollection();
                    _verbs.Add(new DesignerVerb("激活/取消激活", new EventHandler(OnActivateCircle)));
                    _verbs.Add(new DesignerVerb("设计器...", new EventHandler(OnShowDesigner)));
                    _verbs.Add(new DesignerVerb("移动", (s, e) => { ((ControlBase)Control).DesignerMove = !((ControlBase)Control).DesignerMove; }));
                }
                return _verbs;
            }
        }

        public bool Designing { get; set; } = true;

        protected override void OnPaintAdornments(PaintEventArgs pe)
        {
            base.OnPaintAdornments(pe);
            var p = System.Windows.Forms.Control.MousePosition;
            var g = pe.Graphics;
            var rec = pe.ClipRectangle;
            //  rec.Height = rec.Height - 1;
            //rec.Width = rec.Width - 1;
            //  Pen pen = new Pen(Color.Red, 2);
            // pen.Alignment = System.Drawing.Drawing2D.PenAlignment.Inset;
            //    g.DrawEllipse(pen, rec);
            //  g.DrawString("asdfasdfasfasf", Control.Font, Brushes.Red, new PointF(0, 0));
            //if (Designing)
            //{
            //    g.DrawLine(Pens.Blue, 0, 0, 500, 500);
            //}
        }

        protected override void OnMouseEnter()
        {
            base.OnMouseEnter();
            Designing = true;
        }

        protected override void OnMouseLeave()
        {
            base.OnMouseLeave();
            Designing = false;
        }

        public override SelectionRules SelectionRules
        {
            get
            {
                SelectionRules sr = base.SelectionRules;
                if (((ControlBase)Control).DesignerMove)
                {
                    sr |= SelectionRules.Moveable;
                }
                else
                {
                    sr &= ~SelectionRules.Moveable;
                }
                return sr;
            }
        }

        protected override void OnMouseDragMove(int x, int y)
        {
            Designing = true;
            Console.WriteLine("OnMouseDragMove");
            //  ControlBase ab = (ControlBase)this.Control;
            //  PointF point = ab.PointToClient(System.Windows.Forms.Control.MousePosition);
            //  if (point.X < 50 || point.Y < 50)
            //  {
            base.OnMouseDragMove(x, y);
            //}
            //  else
            //  {
            ControlBase ctr = (ControlBase)this.Control;
            var part = ctr.FindForm();
            ctr.Text = part.Text;
            if (!((ControlBase)ctr).DesignerMove)
            {
                var p = ctr.PointToClient(new System.Drawing.Point(x, y));

                GetPropertyByName("BackGroundCenterPoint").SetValue(ctr, p);
            }
            // }
        }

        private void OnShowDesigner(object sender, EventArgs e)
        {
            System.Windows.Forms.MessageBox.Show("Test");
        }

        private void OnActivateCircle(object sender, EventArgs e)
        {
            System.Windows.Forms.MessageBox.Show("Test");
        }

        protected override void OnMouseDragBegin(int x, int y)
        {
            base.OnMouseDragBegin(x, y);

            Console.WriteLine("OnMouseDragBegin");
        }

        /// <summary>
        /// 获取正在设计的控件实例的某一个属性
        /// </summary>
        /// <param name="propName"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentException"></exception>
        private PropertyDescriptor GetPropertyByName(String propName)
        {
            PropertyDescriptor prop;
            prop = TypeDescriptor.GetProperties(Control)[propName];
            if (null == prop)
                throw new ArgumentException("组件未定义该属性。", propName);
            else
                return prop;
        }
    }
}